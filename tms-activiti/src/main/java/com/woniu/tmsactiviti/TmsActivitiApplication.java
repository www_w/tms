package com.woniu.tmsactiviti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(
        exclude = {
                org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
                org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class
        },
        scanBasePackages = "com.woniu"
)
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.woniu.tmscommons.feign")
public class TmsActivitiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmsActivitiApplication.class, args);
    }

}
