package com.woniu.tmsactiviti.config;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;

public class SuspendAndActivateLeave {
    @Resource
    private RepositoryService repositoryService;

    /**
     * 每个月的最后一天挂起请假流程
     */
    @Scheduled(cron = "0 0 0 L * ?")
    public void suspendLeaveProcess() {
        // 通过流程定义的key去数据库ACT_RE_PROCDEF表查询流程对象
        ProcessDefinition definition = repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionKey("employeeLeave")
                .singleResult();
        if (!definition.isSuspended()) {
            // 挂起流程
            repositoryService.suspendProcessDefinitionById(definition.getId(), true, null);
            System.out.println(definition.getId() + " 挂起成功");
        }
    }

    /**
     * 每个月的第一天激活请假流程
     */
    @Scheduled(cron = "0 0 0 1 * ?")
    public void activateLeaveProcess() {
        // 通过流程定义的key去数据库ACT_RE_PROCDEF表查询流程对象
        ProcessDefinition definition = repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionKey("employeeLeave")
                .singleResult();
        if (definition.isSuspended()) {
            // 激活流程
            repositoryService.activateProcessDefinitionById(definition.getId(), true, null);
            System.out.println(definition.getId() + " 激活成功");
        }
    }
}
