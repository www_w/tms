package com.woniu.tmsactiviti.controller;

import com.woniu.tmsactiviti.entity.Cancel;
import com.woniu.tmsactiviti.entity.CustomHistory;
import com.woniu.tmsactiviti.entity.EmployeeLeave;
import com.woniu.tmsactiviti.service.EmployeeLeaveService;
import com.woniu.tmsactiviti.task.CustomTask;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmscommons.utils.JWTUtil;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/employeeLeave")
public class EmployeeLeaveController {
    @Resource
    private EmployeeLeaveService employeeLeaveService;

    /**
     * 申请请假
     * @return ResponseResult
     */
    @PostMapping("/apply")
    public ResponseResult<Boolean> apply(@RequestBody EmployeeLeave employeeLeave, HttpServletRequest request){
        String token = request.getHeader("authorization");
        int uid = JWTUtil.getUid(token);
        employeeLeave.setEmpId(uid);
        Boolean result = employeeLeaveService.apply(employeeLeave);
        return new ResponseResult<>(HttpStatus.OK.value(), "申请成功",result);
    }
    /**
     * 根据员工id查询出待办的任务
     */
    @GetMapping("/findTaskByEmpId/{empId}")
    public ResponseResult<List<CustomTask>> findTaskByEmpId(@PathVariable Integer empId){
        List<CustomTask> tasks = employeeLeaveService.findTaskByEmpId(empId);
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功",tasks);
    }
    @GetMapping("/findTaskByEmpId")
    public ResponseResult<List<CustomTask>> findTaskByEmpId(HttpServletRequest request){
        String token = request.getHeader("authorization");
        int empId = JWTUtil.getUid(token);
        List<CustomTask> tasks = employeeLeaveService.findTaskByEmpId(empId);
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功",tasks);
    }

    /**
     * 实现审批和驳回
     */
    @GetMapping("/completeTask/{taskId}/{agree}")
    public ResponseResult<Boolean> completeTask(@PathVariable("taskId") String taskId,@PathVariable("agree")boolean agree){
        Boolean result = employeeLeaveService.completeTask(taskId,agree);
        return new ResponseResult<>(HttpStatus.OK.value(), "审批成功",result);
    }

    /**
     * 取消请假
     */
    @PostMapping("/cancel")
    public ResponseResult<Boolean> cancel(@RequestBody Cancel cancel){
        return new ResponseResult<>(HttpStatus.OK.value(),"取消请假成功",employeeLeaveService.cancel(cancel));
    }

    /**
     * 根据员工id查询请假历史记录
     * @param request
     * @return
     */
    @GetMapping("/findHistoryById")
    public ResponseResult<List<CustomHistory>> findHistoryById(HttpServletRequest request){
        String token = request.getHeader("authorization");
        int id = JWTUtil.getUid(token);
        return new ResponseResult<>(HttpStatus.OK.value(),"查询历史请假信息成功",employeeLeaveService.findHistoryById(id));
    }

    @GetMapping("/getProcessStatus/{processInstanceId}")
    public ResponseResult<?> getProcessStatus(HttpServletRequest request,@PathVariable("processInstanceId") String processInstanceId){
        // 查询历史
        String token = request.getHeader("authorization");
        int id = JWTUtil.getUid(token);
        List<CustomHistory> CustomHistory = employeeLeaveService.findHistoryById(id);
        //调用方法，返回树形数据
        List<Map<String, Object>> tree =employeeLeaveService.getProcessStatus(CustomHistory,processInstanceId);
        return new ResponseResult<>(HttpStatus.OK.value(),"查看流程状态成功",tree);
    }

}
