package com.woniu.tmsactiviti.entity;

import lombok.Data;

@Data
public class Cancel {
    private String pid;         // 流程id
    private String reason;      // 撤销原因
}