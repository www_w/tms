package com.woniu.tmsactiviti.entity;

import com.woniu.tmscommons.entity.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Compensation implements Serializable {
    private static final long serialVersionUID = -560910873851942279L;
    // 司机id
    private Integer driverId;
    // 订单id
    private String orderId;
    // 货物名称
    private String goodsName;
    // 损坏程度 10%
    private BigDecimal level;
    // 金额
    private Double money;
    // 赔偿原因
    private String reason;
    // 状态
    private String status;
    // 图片链接
    private String url;
    // 审批人id
    private Integer leaderId;
    // 领导id
    private Integer managerId;
    // 货物信息
    private List<OrderItem> orderItems;
}
