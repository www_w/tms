package com.woniu.tmsactiviti.entity;

import com.woniu.tmsactiviti.task.CustomTask;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class CustomHistory {
    private String id;              // 流程id
    private boolean isFinished;     // 是否完成
    private List<CustomTask> customTaskList = new ArrayList<>();    // 任务列表
}