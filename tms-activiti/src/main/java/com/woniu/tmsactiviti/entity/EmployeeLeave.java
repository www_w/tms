package com.woniu.tmsactiviti.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeLeave implements Serializable {
    private static final long serialVersionUID = -560910873851942278L;
    // 员工id
    private Integer empId;
    // 开始时间
    private String startTime;
    // 结束时间
    private String endTime;
    // 请假原因详情
    private String reason;
    // 请假类型
    private String type;
    // 请假状态
    private String status;
    // 请假天数
    private Integer days;
    // 组长id
    private Integer leaderId;
    // 主管id
    private Integer managerId;

}
