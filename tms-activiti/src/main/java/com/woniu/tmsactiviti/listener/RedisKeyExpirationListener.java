package com.woniu.tmsactiviti.listener;

import com.woniu.tmsactiviti.service.EmployeeLeaveService;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * RedisKeyExpirationListener
 */
public class RedisKeyExpirationListener implements MessageListener {

    @Resource
    private EmployeeLeaveService employeeLeaveService;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 获取过期键
        String expiredKey = message.toString();
        System.out.println("Expired Key: " + expiredKey);

        // 根据过期键执行取消请假的业务逻辑
        handleExpiredLeaveRequest(expiredKey);
    }

    private void handleExpiredLeaveRequest(String expiredKey) {
        // 从 Redis key 中解析出 processInstanceId
        String processInstanceId = expiredKey.replace("leave:", "");

        // 执行取消请假的操作
        System.out.println("处理过期的请假申请，processInstanceId: " + processInstanceId);
        // 你可以调用一个服务来处理这个过期的请假申请
        employeeLeaveService.autoCancel(processInstanceId);
    }
}
