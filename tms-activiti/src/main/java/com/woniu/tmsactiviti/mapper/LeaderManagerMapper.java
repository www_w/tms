package com.woniu.tmsactiviti.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LeaderManagerMapper {
    Integer findLeaderId(Integer empId);

    Integer findManagerId(Integer empId);
}
