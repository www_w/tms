package com.woniu.tmsactiviti.service;

import com.woniu.tmsactiviti.entity.Cancel;
import com.woniu.tmsactiviti.entity.Compensation;
import com.woniu.tmsactiviti.entity.CustomHistory;
import com.woniu.tmsactiviti.task.CustomTask;

import java.util.List;
import java.util.Map;

public interface CompensationService {
    Boolean apply(Compensation compensation);

    List<CustomTask> findTaskByEmpId(Integer empId);

    List<CustomHistory> findHistoryById(int id);

    Boolean cancel(Cancel cancel);

    Boolean completeTask(String taskId, boolean agree);

    List<Map<String, Object>> getProcessStatus(List<CustomHistory> customHistory, String processInstanceId);

}
