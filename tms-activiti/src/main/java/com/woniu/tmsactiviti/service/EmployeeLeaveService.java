package com.woniu.tmsactiviti.service;

import com.woniu.tmsactiviti.entity.Cancel;
import com.woniu.tmsactiviti.entity.CustomHistory;
import com.woniu.tmsactiviti.entity.EmployeeLeave;
import com.woniu.tmsactiviti.task.CustomTask;

import java.util.List;
import java.util.Map;

public interface EmployeeLeaveService {
    Boolean apply(EmployeeLeave employeeLeave);

    List<CustomTask> findTaskByEmpId(Integer empId);

    Boolean completeTask(String taskId, boolean agree);

    Boolean cancel(Cancel cancel);

    List<CustomHistory> findHistoryById(int id);

    List<Map<String, Object>> getProcessStatus(List<CustomHistory> customHistory, String processInstanceId);

    void autoCancel(String processInstanceId);
}
