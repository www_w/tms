package com.woniu.tmsactiviti.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.woniu.tmsactiviti.entity.Cancel;
import com.woniu.tmsactiviti.entity.CustomHistory;
import com.woniu.tmsactiviti.entity.EmployeeLeave;
import com.woniu.tmsactiviti.mapper.LeaderManagerMapper;
import com.woniu.tmsactiviti.service.EmployeeLeaveService;
import com.woniu.tmsactiviti.task.CustomTask;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.*;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.persistence.entity.HistoricDetailVariableInstanceUpdateEntity;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class EmployeeLeaveServiceImpl implements EmployeeLeaveService {
    @Resource
    private LeaderManagerMapper leaderManagerMapper;
    @Resource
    private TaskService taskService;
    @Resource
    private HistoryService historyService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RedisTemplate<String, EmployeeLeave> redisTemplate;

    @Override
    public Boolean apply(EmployeeLeave employeeLeave) {
        //1.1 通过员工id查询到员工的组长id，及 主管id
        Integer leaderId = leaderManagerMapper.findLeaderId(employeeLeave.getEmpId());
        Integer managerId = leaderManagerMapper.findManagerId(employeeLeave.getEmpId());
        employeeLeave.setLeaderId(leaderId);
        employeeLeave.setManagerId(managerId);
        //1.2 封装数据：流程中需要用到的变量，对象要求放在map中，流程表达式就是通过key找到变量
        Map<String, Object> args = new HashMap<>();
        args.put("employeeLeave", employeeLeave);
        // 设置当前用户
        Authentication.setAuthenticatedUserId(String.valueOf(employeeLeave.getEmpId()));

        //1.3 启动流程
        ProcessInstance startProcessInstance = runtimeService.startProcessInstanceByKey("employeeLeave", args);
        List<Task> tasks = taskService.createTaskQuery()
                .processDefinitionKey("employeeLeave")
                .taskAssignee("" + employeeLeave.getEmpId())
                .list();
        taskService.complete(tasks.get(tasks.size() - 1).getId());

        // 1.4 获取流程ID
        String processInstanceId = startProcessInstance.getId();
        // 1.5 获取当前时间和请假开始时间，计算时间差
        LocalDateTime now = LocalDateTime.now();
        OffsetDateTime offsetDateTime = OffsetDateTime.parse(employeeLeave.getStartTime(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        // 将 OffsetDateTime 转换为北京时间（Asia/Shanghai）
        ZonedDateTime startTime = offsetDateTime.atZoneSameInstant(ZoneId.of("Asia/Shanghai"));
        long delay = Duration.between(now, startTime.toLocalDateTime()).toMillis();
        System.out.println("Current Time: " + now);
        System.out.println("Start Time: " + startTime.toLocalDateTime());
        System.out.println("Delay: " + delay);
        if (delay > 0) {
            // 1.6 将流程ID与申请信息存入Redis，并设置过期时间
            System.out.println("Storing to Redis with key: leave:" + processInstanceId);

            redisTemplate.opsForValue().set("leave:" + processInstanceId, employeeLeave, delay, TimeUnit.MILLISECONDS);
        } else {
            System.out.println("Delay is not positive, not storing to Redis.");
        }
        return true;
    }

    @Override
    public List<CustomTask> findTaskByEmpId(Integer empId) {
        // 查询当前用户的待办任务
        List<Task> taskList = taskService.createTaskQuery()
                .processDefinitionKey("employeeLeave")
                .taskAssignee("" + empId)
                .list();
        List<CustomTask> customTaskList = new ArrayList<>();
        // 历史查询对象
        HistoricVariableInstanceQuery query = historyService.createHistoricVariableInstanceQuery();
        for (Task task : taskList) {
            // 基本信息
            CustomTask customTask = new CustomTask(task);
            // 添加到集合
            customTaskList.add(customTask);
            // 请假详情：在流程变量中获取，通过任务id
            query.taskId(task.getId());
            // 查询
            List<HistoricVariableInstance> list = query.list();
            // 遍历
            for (HistoricVariableInstance instance : list) {
                // 获取流程变量名称
                String name = instance.getVariableName();
                // 获取流程变量值
                Object value = instance.getValue();
                // 封装
                customTask.getDetails().put(name, value);
            }
        }
        // 返回数据
        return customTaskList;
    }

    @Override
    public Boolean completeTask(String taskId, boolean agree) {
        // 设置流程变量
        taskService.setVariableLocal(taskId, "agreeResult", agree ? "Y" : "N");
        // 完成任务
        taskService.complete(taskId);
        return true;
    }

    @Override
    public Boolean cancel(Cancel cancel) {
        // 参数1：流程id
        // 参数2：撤销原因
        runtimeService.deleteProcessInstance(cancel.getPid(), cancel.getReason());
        return true;
    }

    @Override
    public void autoCancel(String processInstanceId) {
        runtimeService.deleteProcessInstance(processInstanceId, "自动撤销");
    }

    @Override
    public List<CustomHistory> findHistoryById(int id) {
        ObjectMapper objectMapper = new ObjectMapper();
        // System.out.println("1.开始查询历史流程");
        List<HistoricProcessInstance> list = historyService
                .createHistoricProcessInstanceQuery()
                .processDefinitionKey("employeeLeave")
                .startedBy(String.valueOf(id))
                .list();
        // 创建list保存所有记录
        List<CustomHistory> customHistories = new ArrayList<>();
        // 遍历
        // System.out.println("================遍历流程实例================start");
        list.forEach(processInstance -> {
            // 创建流程对象
            CustomHistory customHistory = new CustomHistory();
            // 获取流程id
            String processInstanceId = processInstance.getId();
            // 设置流程id
            customHistory.setId(processInstanceId);

            // System.out.println("2.通过流程id查询流程实例，判断是否结束");
            ProcessInstance instance = runtimeService
                    .createProcessInstanceQuery()
                    .processInstanceId(processInstanceId)
                    .singleResult();

            // 判断
            customHistory.setFinished(instance == null ? true : false);

            // System.out.println("3.根据流程id查询任务列表");
            List<HistoricTaskInstance> taskInstances = historyService.createHistoricTaskInstanceQuery()
                    .processInstanceId(processInstanceId)
                    .orderByHistoricTaskInstanceStartTime().asc()
                    .list();
            // System.out.println("----------------遍历历史任务----------------start");
            taskInstances.forEach(taskInstance -> {
                // 创建任务对象，封装数据
                CustomTask customTask = new CustomTask(taskInstance);

                // System.out.println("4.根据任务id查询任务详情");
                List<HistoricDetail> details = historyService
                        .createHistoricDetailQuery()
                        .taskId(taskInstance.getId())
                        .list();
                // 遍历
                // System.out.println("................遍历任务详情信息.................start");
                details.forEach(detail -> {
                    HistoricDetailVariableInstanceUpdateEntity entity =
                            (HistoricDetailVariableInstanceUpdateEntity) detail;
                    // 获取详情信息
                    String name = entity.getName();
                    String textValue = entity.getTextValue();
                    // 获取值的类型
                    String type = entity.getTextValue2();

                    // 根据类型创建对象
                    try {
                        Class<?> clazz = Class.forName(type);
                        Object obj = objectMapper.readValue(textValue, clazz);
                        // 封装到详情中
                        customTask.getDetails().put(name, obj);
                    } catch (Exception e) {
                        customTask.getDetails().put(name, textValue);
                    }
                    // 判断之前有任务id与现在相同，就不放进去
                    boolean flag = true;
                    for (CustomTask task : customHistory.getCustomTaskList()) {
                        if (task.getId().equals(customTask.getId())) {
                            flag = false;
                        }
                    }
                    if (flag) {
                        customHistory.getCustomTaskList().add(customTask);
                    }
                });
            });
            customHistories.add(customHistory);
        });
        return customHistories;
    }

    @Override
    public List<Map<String, Object>> getProcessStatus(List<CustomHistory> customHistory, String processInstanceId) {
        List<Map<String, Object>> statusList = new ArrayList<>();
        // 遍历 CustomHistory
        for (CustomHistory history : customHistory) {
            // 检查 processInstanceId 是否匹配
            if (history.getId().equals(processInstanceId)) {
                List<CustomTask> customTaskList = history.getCustomTaskList();
                for (CustomTask task : customTaskList) {
                    // 创建流程步骤的映射
                    Map<String, Object> step = new HashMap<>();
                    step.put("name", task.getName());
                    step.put("timestamp", task.getEndTime() != null ? task.getEndTime() : task.getStartTime());
                    step.put("completed", task.getEndTime() != null);
                    step.put("agreeResult", task.getDetails().get("agreeResult")); // 根据你存储的结果字段名来获取
                    statusList.add(step);
                }
                break; // 找到对应的 processInstanceId 后结束循环
            }
        }

        return statusList;
    }


}
