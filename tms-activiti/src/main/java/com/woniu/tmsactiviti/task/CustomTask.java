package com.woniu.tmsactiviti.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomTask {
    // 任务id
    private String id;
    // 流程实例id
    private String processInstanceId;
    // 任务负责人id
    private String assignee;
    // 任务名称
    private String name;
    // 开始时间
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date startTime;
    // 结束时间
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date endTime;
    // 详情
    private Map<String,Object> details = new HashMap<>();

    // 通过任务创建自定义任务
    public CustomTask(Task task){
        this.id = task.getId();
        this.processInstanceId = task.getProcessInstanceId();
        this.assignee = task.getAssignee();
        this.name = task.getName();
    }

    // 通过历史任务示例创建任务
    public CustomTask(HistoricTaskInstance instance){
        this.id = instance.getId();
        this.processInstanceId = instance.getProcessInstanceId();
        this.name = instance.getName();
        this.startTime = instance.getStartTime();
        this.endTime = instance.getEndTime();
        this.assignee = instance.getAssignee();
    }
}