package com.woniu.tmsactiviti;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TmsActivitiApplicationTests {

    /**
     * 部署员工请假流程
     */
    @Test
    void contextLoads() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deployment = repositoryService.createDeployment()
                .name("员工请假申请流程")
                .addClasspathResource("bpmn/employeeLeave.bpmn20.xml")
                .addClasspathResource("bpmn/employeeLeave.png")
                .deploy();
        System.out.println("流程部署id = " + deployment.getId());
        System.out.println("流程部署name = " + deployment.getName());
    }
    @Test
    void contextLoads2() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deployment = repositoryService.createDeployment()
                .name("货物损坏赔偿申请流程")
                .addClasspathResource("bpmn/compensation.bpmn20.xml")
                .addClasspathResource("bpmn/compensation.png")
                .deploy();
        System.out.println("流程部署id= " + deployment.getId());
        System.out.println("流程部署name= " + deployment.getName());
    }

}
