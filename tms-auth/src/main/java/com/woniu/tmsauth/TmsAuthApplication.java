package com.woniu.tmsauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient // 注册到注册中心
@EnableFeignClients(basePackages = "com.woniu.tmscommons.feign") // Feign
public class TmsAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmsAuthApplication.class, args);
    }
}
