package com.woniu.tmsauth.service;

import com.woniu.tmscommons.entity.Menu;

import java.util.List;

public interface MenuService {
    List<Menu> queryMenusByUid(Integer id);
}
