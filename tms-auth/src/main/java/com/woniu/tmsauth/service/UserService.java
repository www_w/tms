package com.woniu.tmsauth.service;



import com.woniu.tmsauth.vo.LoginVo;
import com.woniu.tmsauth.vo.UserVo;
import com.woniu.tmscommons.entity.User;

import java.util.List;

public interface UserService {
    UserVo login(LoginVo vo);

    List<String> findPerms(String token);
}
