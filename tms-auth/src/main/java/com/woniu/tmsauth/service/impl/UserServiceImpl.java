package com.woniu.tmsauth.service.impl;


import com.woniu.tmsauth.service.mapper.UserMapper;
import com.woniu.tmsauth.service.UserService;
import com.woniu.tmsauth.vo.LoginVo;
import com.woniu.tmsauth.vo.UserVo;
import com.woniu.tmscommons.enums.TokenEnum;
import com.woniu.tmscommons.utils.JWTUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import com.woniu.tmscommons.exception.AuthException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public UserVo login(LoginVo vo) {
        // 通过账号查询用户对象
        UserVo user = userMapper.findByAccount(vo.getAccount());
        //
        if (user == null || !user.getPassword().equals(vo.getPassword())){
            throw new AuthException("账号或者密码有误");
        }
        return user;
    }

    @Override
    public List<String> findPerms(String token) {
        if (JWTUtil.verify(token) == TokenEnum.TOKEN_BAD){
            throw new AuthException("token已过期");
        }
        // 查询权限
        return userMapper.findPerms(JWTUtil.getUid(token));
    }
}
