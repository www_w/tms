package com.woniu.tmsauth.service.mapper;



import com.woniu.tmsauth.vo.UserVo;
import com.woniu.tmscommons.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    UserVo findByAccount(String account);

    List<String> findPerms(int uid);
}
