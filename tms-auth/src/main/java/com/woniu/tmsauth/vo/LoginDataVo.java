package com.woniu.tmsauth.vo;

import com.woniu.tmscommons.entity.Menu;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDataVo {
    private UserVo userInfo;
    private List<Menu> menu;
}
