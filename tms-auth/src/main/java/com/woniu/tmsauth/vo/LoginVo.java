package com.woniu.tmsauth.vo;

import lombok.Data;

@Data
public class LoginVo {
    private String account;
    private String password;
}
