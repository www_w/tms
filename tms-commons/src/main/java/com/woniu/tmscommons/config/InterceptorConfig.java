package com.woniu.tmscommons.config;


import com.woniu.tmscommons.interceptor.AuthInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * 拦截器配置类：将拦截器加入到拦截器列表、指定拦截哪些请求
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Resource
    private AuthInterceptor authInterceptor;
    // 将指定的拦截器注册到拦截器列表
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册 、拦截哪些请求
        registry.addInterceptor(authInterceptor).addPathPatterns("/**");
    }
}
