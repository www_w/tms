package com.woniu.tmscommons.entity;

import com.woniu.tmscommons.enums.PayStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Bill implements Serializable {
    private Integer id;
    private BigDecimal totalCost;
    private BigDecimal baseCost;
    private BigDecimal urgentFee;
    private BigDecimal refrigerationFee;
    private BigDecimal insuranceFee;
    private PayStatusEnum payStatus;
    private static final long serialVersionUID = 1L;
}