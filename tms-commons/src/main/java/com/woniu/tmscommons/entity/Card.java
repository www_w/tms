package com.woniu.tmscommons.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-09-06 12:11
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Card implements Serializable {
    private Integer id;
    private String cardNo;
    private BigDecimal balance;
    private Integer status;
    private Integer userId;
    private String cardName;
    private String cardPhone;
}
