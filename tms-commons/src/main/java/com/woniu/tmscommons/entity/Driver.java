package com.woniu.tmscommons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.woniu.tmscommons.enums.DriverStatus;
import com.woniu.tmscommons.enums.DriverTypeStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Driver {
    private Integer id;
    private String name;
    private String phone;
    private String licenseNumber;
    private DriverStatus driverStatus;
    private DriverTypeStatus driverType;
    private Integer userId;
}
