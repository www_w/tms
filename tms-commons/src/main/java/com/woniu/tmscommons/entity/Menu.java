package com.woniu.tmscommons.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.List;

@Data
public class Menu {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private String url;
    private Integer pid;
    private String type;
    private String icon;
    @TableField(exist = false)
    private List<Menu> children;
    @TableField(exist = false)
    private List<String> breadcrumb;
}