package com.woniu.tmscommons.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.woniu.tmscommons.enums.OrderStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author lio
 * @TableName order
 * 订单表
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "\"order\"")
public class Order implements Serializable {
    // 定义订单的唯一标识
//    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    // 订单号，唯一标识一个订单
    private String orderNo;

    // 订单总价
    private BigDecimal totalWorth;

    // 订单状态，如待处理、运输中、已完成等
    private OrderStatusEnum status;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    // 订单创建时间，记录订单被创建的准确时间点
    private LocalDateTime createdAt;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    // 订单更新时间，记录订单信息最后一次被修改的时间点
    private LocalDateTime updatedAt;

    // 司机ID，关联司机信息
    private Integer driverId;

    // 车辆ID，关联车辆信息
    private Integer vehicleId;

    // 收发货方ID，关联发货方客户信息
    private Integer senderReceiverId;

    // 订单总重量，以公斤为单位
    private BigDecimal totalWeight;

    // 是否紧急订单，标识订单是否需要优先处理
    private Boolean isUrgent;

    // 是否冷藏订单，标识订单物品是否需要冷藏运输
    private Boolean isRefrigerated;

    // 保险价值，订单投保的金额
    private Boolean isInsurance;

    // 运输距离，起运地到目的地的公里数
    private BigDecimal distance;

    // 账单ID，关联账单信息
    private Integer billId;

    // 序列化版本UID，用于标识 Serializable 类的不同版本
    private static final long serialVersionUID = 1L;


}