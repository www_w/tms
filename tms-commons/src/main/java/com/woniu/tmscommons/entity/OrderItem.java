package com.woniu.tmscommons.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.math.BigDecimal;
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL) // 仅包括非 null 的字段
public class OrderItem implements Serializable {

    @JsonProperty("id") // JSON 中的字段名称
    private Integer id;

    @JsonProperty("orderId")
    private Integer orderId;

    @JsonProperty("weight")
    private BigDecimal weight;

    @JsonProperty("quantity")
    private Integer quantity;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("goodsName")
    private String goodsName;

    @JsonProperty("worth")
    private BigDecimal worth;

    private static final long serialVersionUID = 1L;
}