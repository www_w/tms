package com.woniu.tmscommons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @Author：田江涛
 * @Package：com.woniu.tmscommons.entity
 * @Project：TMS
 * @name：Perm
 * @Date：2024/8/26 14:57
 * @Filename：Perm
 * @Description:
 */
@Data
public class Perms {
    private Integer id;
    private String permName;
    private String description;
}
