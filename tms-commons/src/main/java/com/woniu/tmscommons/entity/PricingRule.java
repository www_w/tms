package com.woniu.tmscommons.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.checkerframework.common.value.qual.BoolVal;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-08-27 0:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PricingRule implements Serializable {
    private Integer id;
    private Integer vehicleTypeId;
    private BigDecimal minDistance;
    private BigDecimal maxDistance;
    private BigDecimal ratePerKm;
    private static final long serialVersionUID = 1L;
}
