package com.woniu.tmscommons.entity;

import lombok.Data;

/**
 * @Author：田江涛
 * @Package：com.woniu.tmscommons.entity
 * @Project：TMS
 * @name：Role
 * @Date：2024/8/26 14:57
 * @Filename：Role
 * @Description:
 */
@Data
public class Role {
    private Integer id;
    private String roleName;
    private String description;
}
