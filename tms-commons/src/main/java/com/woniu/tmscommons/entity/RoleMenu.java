package com.woniu.tmscommons.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author：田江涛
 * @Package：com.woniu.tmscommons.entity
 * @Project：TMS
 * @name：RoleMenu
 * @Date：2024/9/3 9:53
 * @Filename：RoleMenu
 * @Description:
 */
@Data
@Accessors(chain = true)
public class RoleMenu {
    private Integer roleId;
    private Integer menuId;
}
