//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.woniu.tmscommons.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SenderReceiver implements Serializable {
    private Integer id;
    private String senderName;
    private String senderPhone;
    private String senderAddress;
    private String senderLongitude;//经度
    private String senderLatitude;//纬度
    private String receiverName;
    private String receiverEmail;
    private String receiverPhone;
    private String receiverAddress;
    private String receiverLongitude;//经度
    private String receiverLatitude;//纬度
    private static final long serialVersionUID = 1L;
}
