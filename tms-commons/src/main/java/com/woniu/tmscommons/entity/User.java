package com.woniu.tmscommons.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@TableName("\"user\"")
public class User {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String account;
    // 对象与JSON进行互相转换时忽略该属性
    private String password;
    private String email;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;
    private String avatar;
    @TableField(exist = false)
    private String roleName;
    @TableField(exist = false)
    private Integer roleId;
}
