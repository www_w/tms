package com.woniu.tmscommons.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author：田江涛
 * @Package：com.woniu.tmscommons.entity
 * @Project：TMS
 * @name：UserRole
 * @Date：2024/9/2 14:52
 * @Filename：UserRole
 * @Description:
 */
@Data
@Accessors(chain = true)
public class UserRole {
    private Integer userId;
    private Integer roleId;
}
