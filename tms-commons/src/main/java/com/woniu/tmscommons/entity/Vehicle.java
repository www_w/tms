package com.woniu.tmscommons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.woniu.tmscommons.enums.VehicleBelongStatus;
import com.woniu.tmscommons.enums.VehicleStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Vehicle {
    private Integer id;
    private String vehicleNumber;
    private Integer vehicleTypeId;
    private BigDecimal capacity;
    private VehicleStatus vehicleStatus;
    private Integer driverId;
    private BigDecimal lag;
    private BigDecimal lng;
    private VehicleBelongStatus vehicleBelong;

    @TableField(exist = false)
    private Driver driver;
    @TableField(exist = false)
    private VehicleType vehicleType;



}
