package com.woniu.tmscommons.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum DriverStatus {
    WORKING("工作中","working"),AVAILABLE("空闲状态","available");


    private final String status;
    @JsonValue
    @EnumValue
    private final String value;


    DriverStatus(String status, String value) {
        this.value= value;
        this.status= status;
    }

    public String getValue() {
        return value;
    }
}
