package com.woniu.tmscommons.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum DriverTypeStatus {
    OUR("自家司机","our"),OTHER("别家司机","other");


    private final String status;
    @JsonValue
    @EnumValue
    private final String value;


    DriverTypeStatus(String status, String value) {
        this.value= value;
        this.status= status;
    }

    public String getValue() {
        return value;
    }
}
