package com.woniu.tmscommons.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author lio
 * @create 2024-08-26 23:04
 */
public enum OrderStatusEnum implements IEnum<String> {
    PENDING("0", "处理中"),
    ON_DELIVERY("1", "配送中"),
    FINISH("2", "已完成"),
    MATCHING("3", "匹配中"),
    TIME_OUT("4", "匹配超时");

    @EnumValue
    private final String value;
    @JsonValue
    private final String desc;

    private OrderStatusEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
    @Override
    public String getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.desc;
    }
}
