package com.woniu.tmscommons.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PayStatusEnum implements IEnum<String> {
    IS_PAY("1", "已经付款"),
    NOT_PAY("0", "未付款");

    @EnumValue
    private final String value;
    @JsonValue
    private final String desc;

    private PayStatusEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
    @Override
    public String getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.desc;
    }
}
