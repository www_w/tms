package com.woniu.tmscommons.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum VehicleBelongStatus {
    OUR("自家车辆","our"),OTHER("外部车辆","other");


    private final String status;
    @JsonValue
    @EnumValue
    private final String value;


    VehicleBelongStatus(String status, String value) {
        this.value= value;
        this.status= status;
    }

    public String getValue() {
        return value;
    }
}
