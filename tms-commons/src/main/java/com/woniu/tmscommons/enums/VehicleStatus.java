package com.woniu.tmscommons.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum VehicleStatus {
    AVAILABLE("空闲状态","available"),INSERVICE("工作状态","in_service");


    private final String status;
    @JsonValue
    @EnumValue
    private final String value;


    VehicleStatus(String status, String value) {
        this.value= value;
        this.status= status;
    }

    public String getValue() {
        return value;
    }
}
