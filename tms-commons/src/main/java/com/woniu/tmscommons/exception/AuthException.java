package com.woniu.tmscommons.exception;

public class AuthException extends RuntimeException {
    public AuthException(String message){
        super(message);
    }
}
