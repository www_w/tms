package com.woniu.tmscommons.exception;

public class KillException extends RuntimeException{
    public KillException(String message){
        super(message);
    }
}
