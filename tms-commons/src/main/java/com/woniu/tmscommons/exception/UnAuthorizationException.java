package com.woniu.tmscommons.exception;

public class UnAuthorizationException extends RuntimeException{
    public UnAuthorizationException(String message){
        super(message);
    }
}
