package com.woniu.tmscommons.exception.handler;


import com.woniu.tmscommons.exception.KillException;
import com.woniu.tmscommons.exception.UnAuthorizationException;
import com.woniu.tmscommons.exception.system.MenuAddException;
import com.woniu.tmscommons.exception.system.MenuNotFoundException;
import com.woniu.tmscommons.response.ResponseResult;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandle {

    @ExceptionHandler(UnAuthorizationException.class)
    public ResponseResult<Object> handlerUnAuthorizationException(Exception e){
        return new ResponseResult<>(HttpStatus.SC_UNAUTHORIZED, e.getMessage(), null);
    }
    // 处理秒杀异常
    @ExceptionHandler(KillException.class)
    public ResponseResult<Object> handlerKillException(Exception e){
        return new ResponseResult<>(500, e.getMessage(), null);
    }
    //处理菜单未找到的异常
    @ExceptionHandler(MenuNotFoundException.class)
    public ResponseResult<Object> handlerMenuNotFoundException(Exception e){
        return new ResponseResult<>(HttpStatus.SC_NOT_FOUND, e.getMessage(), null);
    }
    //处理添加菜单异常
    @ExceptionHandler(MenuAddException.class)
    public ResponseResult<Object> handlerMenuAddException(Exception e){
        return new ResponseResult<>(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage(), null);
    }
}
