package com.woniu.tmscommons.exception.system;

/**
 * @Author：田江涛
 * @Package：com.woniu.tmscommons.exception.system
 * @Project：TMS
 * @name：MenuAddException
 * @Date：2024/9/6 10:33
 * @Filename：MenuAddException
 * @Description:
 */
public class MenuAddException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    private Integer code;
    public MenuAddException(String message) {super(message);}
    public MenuAddException(String message, Integer code) {
        super(message);
        this.code = code;
    }
}
