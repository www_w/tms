package com.woniu.tmscommons.exception.system;

/**
 * @Author：田江涛
 * @Package：com.woniu.tmscommons.exception.system
 * @Project：TMS
 * @name：MenuNotFoundException
 * @Date：2024/9/6 9:39
 * @Filename：MenuNotFoundException
 * @Description:
 */
public class MenuNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    private Integer code;
    public MenuNotFoundException(String message) {super(message);}
    public MenuNotFoundException(String message,Integer code) {
        super(message);
        this.code = code;
    }
}
