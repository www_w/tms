package com.woniu.tmscommons.feign;


import com.woniu.tmscommons.response.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@FeignClient(name = "tms-auth", path = "/user")
public interface FeignAuthService {
    @GetMapping("/findPerms")
    ResponseResult<List<String>> findPerms(@RequestHeader("authorization") String token);
}
