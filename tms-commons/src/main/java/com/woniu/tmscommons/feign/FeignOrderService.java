package com.woniu.tmscommons.feign;

import com.woniu.tmscommons.entity.OrderItem;
import com.woniu.tmscommons.response.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author lio
 * @create 2024-09-02 9:17
 */
@FeignClient(name = "tms-order")
public interface FeignOrderService {
    @GetMapping("/orderItem/findByOrderNo/{orderNo}")
    List<OrderItem> findByOrderNo(@PathVariable("orderNo") String orderNo);

    @GetMapping("/vehiclePosition/getMinDistanceVehicle/{senderAddress}")
    ResponseResult<Integer> getSenderAddressPosition(@PathVariable("senderAddress") String senderAddress);
}
