package com.woniu.tmscommons.feign;

import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmscommons.response.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@FeignClient(name = "tms-vehicle" ,path = "/vehicle")
public interface FeignTransportService {

    /**
     * 查询两点之间的距离,单位m
     * */
    @GetMapping("/transport/findDistance/{latFrom}/{lngFrom}/{latTo}/{lngTo}")
    public ResponseResult<Object> findDistance(@PathVariable("latFrom") String latFrom,
                                               @PathVariable("lngFrom") String lngFrom,
                                               @PathVariable("latTo") String latTo,
                                               @PathVariable("lngTo") String lngTo);
    /**
     * 查询当前处于空闲状态的司机和车辆
     * */
    @GetMapping("/transport/findAvailableDriverAndVehicle")
    public ResponseResult<Object> findAvailableDriverAndVehicle();
    /**
     *
     * 根据ip地址来找到经纬度
     * */
    @GetMapping("/transport/findNearestDriverAndVehicle/{ip}")
    public ResponseResult<Object> findCurrentLatAndLng(@PathVariable("ip") String ip);
    /**
     * 查询离当前目的地最近的司机和车辆
     * */
    @GetMapping("/transport/findShortestDistance/{latTo}/{lngTo}")
    public ResponseResult<Driver> findShortestDistance(@PathVariable("latTo") String latTo,
                                                       @PathVariable("lngTo") String lngTo);

    /**
     * 传一个吨位的数字，匹配一个最合适的车辆信息返还
     * */
    @GetMapping("/transport/findRightVehicle/{capacity}/{senderAddress}")
    public ResponseResult<Vehicle> findRightVehicle(@PathVariable("capacity") Double capacity,@PathVariable("senderAddress") String senderAddress);

    @GetMapping("/driver/selectById/{id}")
    public ResponseResult<Object> selectById(@PathVariable("id") Integer id);

    /**
     * 发送邮件接口
     * */
    @GetMapping("/transport/sendEmail/{email}")
    public ResponseResult<Object> sendEmail(@PathVariable("email") String email);

    /**
     * 发送短信的接口(还未测试，先莫调用)
     * */
    @GetMapping("/sendSms/{phoneNum}")
    public ResponseResult<Object> sendSms(@PathVariable("phoneNum") String phoneNum);
}
