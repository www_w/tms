package com.woniu.tmscommons.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)  // 链式表达
public class ResponseResult<T> {
    private Integer code;
    private String message;
    private T data;
}
