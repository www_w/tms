package com.woniu.tmscommons.utils;

/**
 * @author lio
 * @create 2024-08-27 9:30
 */
public class OrderNoUtil {
    public static String getOrderNo() {
        return "order"+System.currentTimeMillis();
    }
}
