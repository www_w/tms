package com.woniu.tmsgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.woniu",exclude= {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
public class TmsGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmsGatewayApplication.class, args);
    }
}
