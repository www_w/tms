package com.woniu.tmsgateway.filter;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.woniu.tmscommons.enums.TokenEnum;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmscommons.utils.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * GlobalFilter：是一种pre类型的过滤器，全局过滤器可以过滤所有的请求，局部过滤器只能过滤特定的请求，例如以/goods开头的
 * Ordered：指定过滤器的执行顺序
 *
 */
@Slf4j
@Component
public class AuthFilter implements GlobalFilter, Ordered {

    // 执行过滤的方法, 返回Mono对象
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.debug("过滤器执行过滤");
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        String uri = request.getURI().getPath();
        log.debug(uri);

        // 判断uri是否需要登录之后才能访问
        if(requireAuth(uri)){
            // 没匹配上
            // 获取token、校验token
            List<String> list = request.getHeaders().get("authorization");
            if (list == null || list.size() == 0){
                // 没token
                return goLogin(response);
            }

            // 校验token
            String token = list.get(0);
            TokenEnum verify = JWTUtil.verify(token);
            if (verify != TokenEnum.TOKEN_SUCCESS){
                return goLogin(response);
            }
        }

        // 放行
        return chain.filter(exchange);
    }

    /**
     * 去登录
     * @param response
     * @return
     */
    private static Mono<Void> goLogin(ServerHttpResponse response) {
        ResponseResult<Object> result = new ResponseResult<>(HttpStatus.SC_FORBIDDEN,"请登录",null);
        //将数据转换成byte数组
        byte[] data = null;
        try {
            data = new ObjectMapper().writeValueAsString(result).getBytes(StandardCharsets.UTF_8);
        }catch (Exception e){
            e.printStackTrace();
        }
        //创建数据缓存对象
        DataBuffer buffer = response.bufferFactory().wrap(data);
        //设置响应头
        response.getHeaders().add("Content-Type","application/json;charset=utf-8");
        //返回数据
        return response.writeWith(Mono.just(buffer));
    }

    // 执行顺序，数字越小越先执行
    @Override
    public int getOrder() {
        return 0;
    }

    /**
     *
     * @param uri
     * @return  返回true 需要登录，false 不需要
     */
    private boolean requireAuth(String uri){
        // 不需要的罗列出来
        List<String> uriList = Arrays.asList(
                "/user/login","/user/register","/goods/find*/**","/kill/find*/**"
        );
        // spring提供用于进行路径匹配的类
        AntPathMatcher antPathMatcher = new AntPathMatcher();

        AtomicBoolean res = new AtomicBoolean(true);
        // 循环匹配
        uriList.forEach((path) -> {
            if (antPathMatcher.match(path, uri)){
                // 匹配上了
                log.debug("{} 匹配上了 {}", uri, path);

                res.set(false);
                return;
            }
        });
        // 返回结果
        return res.get();
    }
}
