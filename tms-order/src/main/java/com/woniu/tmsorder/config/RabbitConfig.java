package com.woniu.tmsorder.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
   public class RabbitConfig {
    public static final String EMAIL_QUEUE = "email-Queue";
    public static final String EMAIL_EXCHANGE = "email-Exchange";
    public static final String ORDER_PAY_QUEUE = "orderPay-Queue";
    public static final String ORDER_PAY_EXCHANGE = "orderPay-Exchange";
    public static final String ORDER_DEAD_QUEUE = "orderDead-Queue";
    public static final String ORDER_DEAD_EXCHANGE = "orderDead-Exchange";
       //邮件队列
       @Bean
       public Queue emailQueue() {
           return new Queue(EMAIL_QUEUE);
       }
       @Bean
       public FanoutExchange emailExchange() {
           return new FanoutExchange(EMAIL_EXCHANGE);
       }
       @Bean
       public Binding emailBinding() {
           return BindingBuilder.bind(emailQueue()).to(emailExchange());
       }
       //订单支付队列
//       @Bean
//       public Queue orderPayQueue() {
//           Map<String, Object> arguments = new HashMap<>();
//           arguments.put("x-message-ttl", 30*1000);
//           arguments.put("x-dead-letter-exchange", ORDER_DEAD_EXCHANGE);
//           return new Queue(ORDER_PAY_QUEUE, true, false, false, arguments);
//       }
//       @Bean
//       public FanoutExchange orderPayExchange() {
//           return new FanoutExchange(ORDER_PAY_EXCHANGE);
//       }
//       @Bean
//       public Binding orderPayBinding() {
//           return BindingBuilder.bind(orderPayQueue()).to(orderPayExchange());
//       }
//       //订单亡语队列
//       @Bean
//       public Queue orderDeadQueue() {
//           return new Queue(ORDER_DEAD_QUEUE);
//       }
//       @Bean
//       public FanoutExchange orderDeadExchange() {
//           return new FanoutExchange(ORDER_DEAD_EXCHANGE);
//       }
//       @Bean
//       public Binding orderDeadBinding() {
//           return BindingBuilder.bind(orderDeadQueue()).to(orderDeadExchange());
//       }

   }
   