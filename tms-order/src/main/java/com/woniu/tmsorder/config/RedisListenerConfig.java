package com.woniu.tmsorder.config;

import com.woniu.tmsorder.listener.DriverStatusListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;


@Configuration
public class RedisListenerConfig {
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                                   MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter, driverAndVehicleStatusTopic());
        return container;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(DriverStatusListener driverStatusListener) {
        return new MessageListenerAdapter(driverStatusListener);
    }

    @Bean
    public ChannelTopic driverAndVehicleStatusTopic() {
        return new ChannelTopic("driver_vehicle_status");
    }
}