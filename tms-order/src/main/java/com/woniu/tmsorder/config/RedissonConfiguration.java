package com.woniu.tmsorder.config;

import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfiguration {
    @Bean
    public Redisson redisson() {
        //单机模式
        Config config = new Config();
        config.useSingleServer().setAddress("redis://47.120.28.163:6379").setUsername("root").setPassword("root").setDatabase(0);
        return (Redisson)Redisson.create(config);
    }
}