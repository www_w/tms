package com.woniu.tmsorder.controller;

import com.woniu.tmscommons.entity.Bill;
import com.woniu.tmscommons.entity.PricingRule;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsorder.service.BillService;
import com.woniu.tmsorder.vo.PricingRuleVo;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-08-27 17:16
 */
@RestController
@RequestMapping({"/bill"})
public class BillController {
    @Resource
    private BillService billService;
    @PostMapping({"/add"})
    public ResponseResult<Void> add(@RequestBody Bill bill){
        billService.add(bill);
        return new ResponseResult<>(HttpStatus.SC_OK, "生成账单成功", null);
    }



}
