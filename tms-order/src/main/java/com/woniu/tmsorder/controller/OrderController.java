
package com.woniu.tmsorder.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.woniu.tmscommons.entity.Order;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsorder.service.OrderService;
import java.util.List;
import javax.annotation.Resource;

import com.woniu.tmsorder.vo.*;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/order"})
public class OrderController {
    @Resource
    private OrderService orderService;
    @PostMapping({"/add"})
    public ResponseResult<Void> add(@RequestBody OrderVo vo) {
        System.out.println("参数:"+vo);
        orderService.add(vo);
        return new ResponseResult<>(200, "添加订单成功", null);
    }
    @PostMapping({"/addOther"})
    public ResponseResult<Void> addOther(@RequestBody OrderVo vo) {
        System.out.println("参数:"+vo);
        orderService.addOther(vo);
        return new ResponseResult<>(200, "添加订单成功", null);
    }


    @GetMapping({"/find/{orderNo}"})
    public ResponseResult<OrderVo> findByOrderNo(@PathVariable("orderNo") String orderNo) {
        OrderVo order = orderService.findByOrderNo(orderNo);
        if (order == null) {
            return new ResponseResult<>(HttpStatus.SC_NOT_FOUND, "订单不存在", null);
        }
        return new ResponseResult<>(HttpStatus.SC_OK, "查询订单成功", order);
    }

    @DeleteMapping({"/delete/{orderNo}"})
    public ResponseResult<Void> deleteByOrderNo(@PathVariable("orderNo") String orderNo) {
        orderService.deleteByOrderNo(orderNo);
        return new ResponseResult<>(HttpStatus.SC_OK, "删除订单成功", null);
    }

    @PutMapping({"/update"})
    public ResponseResult<Void> update(@RequestBody Order order) {
        this.orderService.update(order);
        return new ResponseResult<>(HttpStatus.SC_OK, "更新订单成功", null);
    }

    @PostMapping({"/list"})
    public ResponseResult<IPage<List<OrderVo>>> findAll(@RequestBody PageVo o) {
        IPage<List<OrderVo>> list = orderService.findAll(o);
        return new ResponseResult<>(HttpStatus.SC_OK, "查询所有订单成功", list);
    }
    @PostMapping("/autoAllocation")
    public ResponseResult<AutoAllocatipnVo> autoAllocation(@RequestBody ConditionVo conditionVo){
        AutoAllocatipnVo result =orderService.autoAllocation(conditionVo);
        return new ResponseResult<>(HttpStatus.SC_OK,"自动分配成功",result);

    }
    @PostMapping("/killOrder")
    public ResponseResult<Boolean> killOrder(@RequestBody OtherDriverVo vo){
        Boolean r = orderService.killOrder(vo);
        return new ResponseResult<>(HttpStatus.SC_OK,"抢单成功",r);
    }
    //获取所有秒杀订单
    @GetMapping("/killList")
    public ResponseResult<List<OrderVo>> KillList(){
        System.out.println("获取所有秒杀订单");
        List<OrderVo> list = orderService.getKillList();
        return new ResponseResult<>(HttpStatus.SC_OK,"查询所有秒杀订单成功",list);
    }
    @PutMapping("/finishOrder/{orderNo}")
    public ResponseResult<Void> finishOrder(@PathVariable("orderNo") String orderNo){
        orderService.finishOrder(orderNo);
        return new ResponseResult<>(HttpStatus.SC_OK,"订单完成",null);
    }
    @GetMapping("/matchAgain/{orderNo}")
    public ResponseResult<Void> matchAgain(@PathVariable("orderNo") String orderNo){
        orderService.matchAgain(orderNo);
        return new ResponseResult<>(HttpStatus.SC_OK,"订单已经重新匹配",null);
    }
}
