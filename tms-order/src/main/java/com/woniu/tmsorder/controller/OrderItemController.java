package com.woniu.tmsorder.controller;

import com.woniu.tmscommons.entity.OrderItem;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsorder.service.OrderItemService;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lio
 * @create 2024-08-27 19:56
 */
@RestController
@RequestMapping("/orderItem")
public class OrderItemController {
    @Resource
    private OrderItemService orderItemService;
    @PostMapping("/add")
    public ResponseResult<Void> add(@RequestBody OrderItem orderItem){
        orderItemService.add(orderItem);

        return new ResponseResult<>(HttpStatus.SC_OK, "添加成功", null);
    }
    @GetMapping("/findByOrderNo/{orderNo}")
    public ResponseResult<List<OrderItem>> findByOrderNo(@PathVariable("orderNo") String orderNo){
        return new ResponseResult<>(HttpStatus.SC_OK, "查询成功", orderItemService.findByOrderNo(orderNo));
    }
}
