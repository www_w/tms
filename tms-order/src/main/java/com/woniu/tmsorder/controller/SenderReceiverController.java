package com.woniu.tmsorder.controller;

import com.woniu.tmscommons.entity.SenderReceiver;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsorder.service.SenderReceiverService;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author lio
 * @create 2024-08-27 15:02
 */
@RestController
@RequestMapping({"/senderReceiver"})
public class SenderReceiverController {
    @Resource
    private SenderReceiverService senderReceiverService;
    @PostMapping({"/add"})
    public ResponseResult<Void> add(@RequestBody SenderReceiver senderReceiver){
        senderReceiverService.add(senderReceiver);
        return new ResponseResult<>(HttpStatus.SC_OK, "添加成功", null);
    }
    @DeleteMapping({"/delete/{id}"})
    public ResponseResult<Void> delete(@PathVariable("id") Integer id){
        senderReceiverService.delete(id);
        return new ResponseResult<>(HttpStatus.SC_OK, "删除成功", null);
    }

}
