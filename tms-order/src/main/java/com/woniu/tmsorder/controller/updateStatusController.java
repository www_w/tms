package com.woniu.tmsorder.controller;

import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsorder.service.updateStatusService;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lio
 * @create 2024-09-04 14:47
 */
@RestController
@RequestMapping("/status")
public class updateStatusController {
    @Resource
    private updateStatusService updateDriverStatus;
    @GetMapping("/updateDriverStatus/{driverId}")
    public ResponseResult<Void> updateDriverStatus(@PathVariable("driverId") Integer driverId) {
        updateDriverStatus.updateDriverStatus(driverId);
        return new ResponseResult<>(HttpStatus.SC_OK, "更新成功", null);
        //调用feign接口，更新司机状态
    }

    /**
     * 结束订单时的状态更新
     * @param vehicleId
     * @return
     */
    @GetMapping("/updateDriverAndVehicleStatusOfAfterFinishOrder/{vehicleId}")
    public ResponseResult<Void> updateDriverAndVehicleStatus(@PathVariable("vehicleId") Integer vehicleId) {
        updateDriverStatus.updateDriverAndVehicleStatus(vehicleId);
        return new ResponseResult<>(HttpStatus.SC_OK, "更新成功", null);
        //调用feign接口，更新司机状态
    }

    /**
     * 开始运输时的状态更新
     * @param vehicleId
     * @return
     */
    @GetMapping("/updateDriverAndVehicleStatusOfAfterStartOrder/{vehicleId}/{driverId}")
    public ResponseResult<Void> updateDriverAndVehicleStatusOfAfterStartOrder(@PathVariable("vehicleId") Integer vehicleId,@PathVariable("driverId") Integer driverId) {
        updateDriverStatus.updateDriverAndVehicleStatusOfAfterStartOrder(vehicleId,driverId);
        return new ResponseResult<>(HttpStatus.SC_OK, "更新成功", null);
        //调用feign接口，更新司机状态
    }
}
