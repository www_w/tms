package com.woniu.tmsorder.controller;

import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsorder.service.vehiclePositionService;
import com.woniu.tmsorder.vo.vehiclePositionVo;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.redis.domain.geo.GeoLocation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author lio
 * @create 2024-09-06 15:10
 */

@RestController
@RequestMapping("/vehiclePosition")
public class vehiclePositionController {
    @Resource
    private vehiclePositionService vehiclePositionService;
    //更新车辆位置信息
    @PostMapping("/updateVehiclePosition")
    public ResponseResult<Void> updateVehiclePosition(vehiclePositionVo vo){
        vehiclePositionService.updateVehiclePosition(vo);
        return new ResponseResult<>(200,"更新成功",null);
    }
    //
    @GetMapping("/addAllVehiclePosition")
    public ResponseResult<Void> addAllVehiclePosition(){
        vehiclePositionService.addAllVehiclePosition();
        return new ResponseResult<>(200,"添加成功",null);
    }
    //
    @GetMapping("/getMinDistanceVehicle/{senderAddress}")
    public ResponseResult<Integer> getSenderAddressPosition(@PathVariable("senderAddress") String senderAddress){
        GeoResults senderAddressPosition = vehiclePositionService.getSenderAddressPosition(senderAddress);
        GeoResult o = (GeoResult) senderAddressPosition.getContent().get(0);
        GeoLocation location = (GeoLocation) o.getContent();
        Integer vehicleId = (Integer) location.getName();
        System.out.println(vehicleId);
        return new ResponseResult<>(200,"添加成功",vehicleId);

    }
}
