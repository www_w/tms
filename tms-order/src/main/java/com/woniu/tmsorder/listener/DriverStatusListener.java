package com.woniu.tmsorder.listener;

import com.alibaba.fastjson.JSONObject;
import com.woniu.tmscommons.enums.DriverStatus;
import com.woniu.tmscommons.enums.VehicleStatus;
import com.woniu.tmsorder.service.OrderService;
import com.woniu.tmsorder.vo.OrderVo;
import com.woniu.tmsorder.vo.OtherDriverVo;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author lio
 * @create 2024-09-03 12:58
 */
@Component
public class DriverStatusListener implements MessageListener {
    @Lazy
    @Resource
    private OrderService orderService;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String payload = message.toString();
        System.out.println("状态改变: " + payload);
        // 进一步处理司机状态变化
        String[] parts = payload.split(":");
        for (String part : parts) {
            System.out.println(part);
        }
        handleDriverStatusChange(payload);
    }
    private void handleDriverStatusChange(String payload) {
        List<OrderVo> killList = orderService.getKillList();
        if (killList.isEmpty()){
            return;
        }
        int i = -1;
        String[] parts = payload.split(":");
        if (parts.length == 7) {
            Object obj = redisTemplate.opsForHash().get("myVehicles", "vehicle:" + parts[3]);
            Object  d = redisTemplate.opsForHash().get("vehicles_drivers", "driver:" + parts[1]);
            if (obj != null&& d != null){
                Integer driverId = Integer.parseInt(parts[1]);
                String driverStatus = parts[2];
                Integer vehicleId = Integer.parseInt(parts[3]);
                String vehicleStatus = parts[4];
                Integer vehicleTypeId = Integer.parseInt(parts[5]);
                if (driverStatus.equals(DriverStatus.AVAILABLE.getValue()) && vehicleStatus.equals(VehicleStatus.AVAILABLE.getValue())) {
                    for (OrderVo orderVo : killList) {
                        if (orderVo.getVehicleId()==null&&orderVo.getDriverId()==null) {
                            i = killList.indexOf(orderVo);
                            break;
                        }
                    }
                    orderService.killOrder(new OtherDriverVo(driverId, vehicleId, killList.get(i).getOrderNo(),vehicleTypeId));
                    redisTemplate.opsForHash().delete("myVehicles", "vehicle:" + vehicleId);
                    redisTemplate.opsForHash().delete("vehicles_drivers", "driver:" + driverId);
                }
            }
        }
        if (parts.length == 4) {
            Object  d = redisTemplate.opsForHash().get("myDrivers", "driver:" + parts[1]);
            if (d != null) {
                Integer driverId = Integer.parseInt(parts[1]);
                String status = parts[2];
                if (status.equals(DriverStatus.AVAILABLE.getValue())) {
                    for (OrderVo orderVo : killList) {
                        if (orderVo.getVehicleId() != null) {
                            i = killList.indexOf(orderVo);
                            break;
                        }
                    }
                    orderService.killOrder(new OtherDriverVo(driverId, killList.get(i).getVehicleId(), killList.get(i).getOrderNo(),null));
                    redisTemplate.opsForHash().delete("myDrivers", "drivers:" + driverId);
                }
            }
        }
    }
}
