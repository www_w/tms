package com.woniu.tmsorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.Bill;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lio
 * @create 2024-08-27 17:21
 */
@Mapper
public interface BillMapper extends BaseMapper<Bill> {
}
