package com.woniu.tmsorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.OrderItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author lio
 * @create 2024-08-27 20:03
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {
    List<OrderItem> findByOrderNo(String orderNo);
}
