package com.woniu.tmsorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.woniu.tmscommons.entity.Order;

import java.util.List;

import com.woniu.tmsorder.vo.OrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
    OrderVo findByOrderNo(String OrderNo);

    void deleteByOrderNo(String OrderNo);

    IPage<List<OrderVo>> findListAndPage(@Param("page") IPage<OrderVo> page, @Param("order") Order order);

    List<OrderVo> findByDriverId(Integer driverId);
}
