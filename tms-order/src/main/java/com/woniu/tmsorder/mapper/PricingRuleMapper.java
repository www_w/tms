package com.woniu.tmsorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.PricingRule;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author lio
 * @create 2024-08-29 16:39
 */
@Mapper
public interface PricingRuleMapper extends BaseMapper<PricingRule> {
}
