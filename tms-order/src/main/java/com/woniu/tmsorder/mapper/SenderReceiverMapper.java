package com.woniu.tmsorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.SenderReceiver;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lio
 * @create 2024-08-27 15:10
 */
@Mapper
public interface SenderReceiverMapper extends BaseMapper<SenderReceiver> {
}
