package com.woniu.tmsorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.Driver;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lio
 * @create 2024-09-04 14:47
 */
@Mapper
public interface updateStatusMapper extends BaseMapper<Driver> {

    String findDriverTypeAndEmail(Integer driverId);
}
