package com.woniu.tmsorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.Vehicle;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lio
 * @create 2024-09-06 15:12
 */
@Mapper
public interface vehiclePositionMapper extends BaseMapper<Vehicle> {

}
