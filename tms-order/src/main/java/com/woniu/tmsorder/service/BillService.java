package com.woniu.tmsorder.service;

import com.woniu.tmscommons.entity.Bill;
import com.woniu.tmsorder.vo.PricingRuleVo;

import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-08-27 17:20
 */
public interface BillService {
    void add(Bill bill);


    void deleteById(Integer orderId);
}
