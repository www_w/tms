package com.woniu.tmsorder.service;

import com.woniu.tmscommons.entity.OrderItem;

import java.util.List;

/**
 * @author lio
 * @create 2024-08-27 20:00
 */
public interface OrderItemService {
    void add(OrderItem orderItem);

    List<OrderItem> findByOrderNo(String orderNo);

    void deleteByOrderId(Integer orderId);
}
