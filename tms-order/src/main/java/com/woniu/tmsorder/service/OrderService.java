//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.woniu.tmsorder.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.woniu.tmscommons.entity.Order;
import com.woniu.tmsorder.vo.*;

import java.util.List;

public interface OrderService {
    void add(OrderVo vo);

    OrderVo findByOrderNo(String OrderNo);

    void deleteByOrderNo(String OrderNo);

    void update(Order order);

    IPage<List<OrderVo>> findAll(PageVo o);

    AutoAllocatipnVo autoAllocation(ConditionVo conditionVo);

    void addOther(OrderVo vo);

    Boolean killOrder(OtherDriverVo vo);

    List<OrderVo> getKillList();


    void finishOrder(String orderNo);

    void updateOrderStatusByOrderNo(String orderNo);

    void matchAgain(String orderNo);
}
