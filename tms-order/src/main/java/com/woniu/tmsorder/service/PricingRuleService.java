package com.woniu.tmsorder.service;

import com.woniu.tmsorder.vo.PricingRuleVo;

import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-08-29 16:38
 */
public interface PricingRuleService {
    BigDecimal getUnitPrice(PricingRuleVo vo);
}
