package com.woniu.tmsorder.service;

import com.woniu.tmscommons.entity.SenderReceiver;

/**
 * @author lio
 * @create 2024-08-27 15:09
 */
public interface SenderReceiverService {
    void add(SenderReceiver senderReceiver);

    void delete(Integer id);

    void deleteById(Integer senderReceiverId);
}
