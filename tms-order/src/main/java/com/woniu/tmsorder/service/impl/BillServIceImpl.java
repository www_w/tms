package com.woniu.tmsorder.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.woniu.tmscommons.entity.Bill;
import com.woniu.tmsorder.mapper.BillMapper;
import com.woniu.tmsorder.service.BillService;
import com.woniu.tmsorder.service.PricingRuleService;
import com.woniu.tmsorder.vo.PricingRuleVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.MathContext;

/**
 * @author lio
 * @create 2024-08-27 17:20
 */
@Service
public class BillServIceImpl implements BillService {
    @Resource
    private BillMapper billMapper;
    @Resource
    private PricingRuleService pricingRuleService;
    @Override
    public void add(Bill bill) {
        billMapper.insert(bill);
    }

    @Override
    public void deleteById(Integer billId) {
        billMapper.deleteById(billId);
    }
}
