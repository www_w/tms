package com.woniu.tmsorder.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.woniu.tmscommons.entity.OrderItem;
import com.woniu.tmsorder.mapper.OrderItemMapper;
import com.woniu.tmsorder.service.OrderItemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lio
 * @create 2024-08-27 20:00
 */
@Service
public class OrderItemServiceImpl implements OrderItemService {
    @Resource
    private OrderItemMapper orderItemMapper;
    @Override
    public void add(OrderItem orderItem) {
        orderItemMapper.insert(orderItem);
    }

    @Override
    public List<OrderItem> findByOrderNo(String orderNo) {
        return orderItemMapper.findByOrderNo(orderNo);
    }

    @Override
    public void deleteByOrderId(Integer orderId) {
        LambdaQueryWrapper<OrderItem> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderItem::getOrderId, orderId);
        orderItemMapper.delete(queryWrapper);
    }
}
