package com.woniu.tmsorder.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.woniu.tmscommons.entity.PricingRule;
import com.woniu.tmsorder.mapper.PricingRuleMapper;
import com.woniu.tmsorder.service.PricingRuleService;
import com.woniu.tmsorder.vo.PricingRuleVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-08-29 16:39
 */
@Service
public class PricingRuleServiceImpl implements PricingRuleService {
    @Resource
    private PricingRuleMapper pricingRuleMapper;
    @Override
    public BigDecimal getUnitPrice(PricingRuleVo vo) {
        LambdaQueryWrapper<PricingRule> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PricingRule::getVehicleTypeId, vo.getVehicle_type_id())
                .le(PricingRule::getMinDistance, vo.getDistance())
                .ge(PricingRule::getMaxDistance, vo.getDistance());
        return pricingRuleMapper.selectOne(queryWrapper).getRatePerKm();
    }
}
