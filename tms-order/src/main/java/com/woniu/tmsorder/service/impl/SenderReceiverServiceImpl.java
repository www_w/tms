package com.woniu.tmsorder.service.impl;

import com.woniu.tmscommons.entity.SenderReceiver;
import com.woniu.tmsorder.mapper.SenderReceiverMapper;
import com.woniu.tmsorder.service.SenderReceiverService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author lio
 * @create 2024-08-27 15:09
 */
@Service
public class SenderReceiverServiceImpl implements SenderReceiverService {
    @Resource
    private SenderReceiverMapper senderReceiverMapper;
    @Override
    public void add(SenderReceiver senderReceiver) {
        senderReceiverMapper.insert(senderReceiver);
    }

    /**
     * 删除
     * @param id
     */
    @Override
    public void delete(Integer id) {
        senderReceiverMapper.deleteById(id);
    }

    @Override
    public void deleteById(Integer senderReceiverId) {
        senderReceiverMapper.deleteById(senderReceiverId);
    }
}
