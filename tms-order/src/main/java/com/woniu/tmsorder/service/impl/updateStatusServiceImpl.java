package com.woniu.tmsorder.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmscommons.enums.DriverStatus;
import com.woniu.tmscommons.enums.VehicleBelongStatus;
import com.woniu.tmscommons.enums.VehicleStatus;
import com.woniu.tmsorder.mapper.OrderMapper;
import com.woniu.tmsorder.mapper.updateStatusMapper;
import com.woniu.tmsorder.mapper.updateVehicleStatusMapper;
import com.woniu.tmsorder.service.OrderService;
import com.woniu.tmsorder.service.updateStatusService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author lio
 * @create 2024-09-04 14:47
 */
@Service
public class updateStatusServiceImpl implements updateStatusService {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private updateStatusMapper updateStatusMapper;
    @Resource
    private updateVehicleStatusMapper updateVehicleStatusMapper;

    /**
     * 更新司机状态
     *
     * @param driverId
     */
    @Override
    public void updateDriverStatus(Integer driverId) {
        LambdaUpdateWrapper<Driver> driverLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        driverLambdaUpdateWrapper.eq(Driver::getId, driverId);
        driverLambdaUpdateWrapper.set(Driver::getDriverStatus, DriverStatus.AVAILABLE);
        updateStatusMapper.update(driverLambdaUpdateWrapper);
        redisTemplate.convertAndSend("driver_vehicle_status", "driver:" + driverId + ":" + DriverStatus.AVAILABLE.getValue() + ":");
    }

    /**
     * 更新车辆和司机状态
     *
     * @param vehicleId
     */
    @GlobalTransactional
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDriverAndVehicleStatus(Integer vehicleId) {
        Vehicle vehicle = updateVehicleStatusMapper.selectById(vehicleId);
        updateDriverStatus(vehicle.getDriverId());
        //根据车辆Id查询车辆并且更新该车的状态
        LambdaUpdateWrapper<Vehicle> vehicleLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        vehicleLambdaUpdateWrapper.eq(Vehicle::getId, vehicleId);
        //更新车辆状态
        vehicleLambdaUpdateWrapper.set(Vehicle::getVehicleStatus, VehicleStatus.AVAILABLE);
        //更新司机Id为null
        if (vehicle.getVehicleBelong() == VehicleBelongStatus.OUR) {
            vehicleLambdaUpdateWrapper.set(Vehicle::getDriverId, null);
        }
        updateVehicleStatusMapper.update(vehicleLambdaUpdateWrapper);
        //发送消息到监听频道
        redisTemplate.convertAndSend("driver_vehicle_status", "driver:" + vehicle.getDriverId() + ":" + DriverStatus.AVAILABLE.getValue()
                + ":" + vehicleId + ":" + VehicleStatus.AVAILABLE.getValue() + ":" + vehicle.getVehicleTypeId() + ":");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDriverAndVehicleStatusOfAfterStartOrder(Integer vehicleId, Integer driverId) {
        //更新司机状态
        LambdaUpdateWrapper<Driver> driverLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        driverLambdaUpdateWrapper.eq(Driver::getId, driverId);
        driverLambdaUpdateWrapper.set(Driver::getDriverStatus, DriverStatus.WORKING);
        updateStatusMapper.update(driverLambdaUpdateWrapper);
        //更新车辆状态
        LambdaUpdateWrapper<Vehicle> vehicleLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        vehicleLambdaUpdateWrapper.eq(Vehicle::getId, vehicleId);
        vehicleLambdaUpdateWrapper.set(Vehicle::getVehicleStatus, VehicleStatus.INSERVICE);
        //司机和车辆绑定
        vehicleLambdaUpdateWrapper.set(Vehicle::getDriverId, driverId);
        updateVehicleStatusMapper.update(vehicleLambdaUpdateWrapper);
    }

    @Override
    public String findDriverTypeAndEmail(Integer driverId) {
        return updateStatusMapper.findDriverTypeAndEmail(driverId);
    }
}
