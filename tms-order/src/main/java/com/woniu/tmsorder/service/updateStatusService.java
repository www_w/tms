package com.woniu.tmsorder.service;

import com.woniu.tmscommons.response.ResponseResult;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lio
 * @create 2024-09-04 14:47
 */

public interface updateStatusService {
        void updateDriverStatus(Integer driverId);

    void updateDriverAndVehicleStatus(Integer vehicleId);

    void updateDriverAndVehicleStatusOfAfterStartOrder(Integer vehicleId, Integer driverId);

    String findDriverTypeAndEmail(Integer driverId);
}
