package com.woniu.tmsorder.service;

import com.woniu.tmsorder.vo.vehiclePositionVo;
import org.springframework.data.geo.GeoResults;

/**
 * @author lio
 * @create 2024-09-06 15:12
 */
public interface vehiclePositionService {
    void updateVehiclePosition(vehiclePositionVo vo);

    void addAllVehiclePosition();

    GeoResults getSenderAddressPosition(String senderAddress);
}
