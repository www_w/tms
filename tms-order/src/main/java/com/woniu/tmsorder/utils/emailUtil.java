package com.woniu.tmsorder.utils;

import com.woniu.tmsorder.pojo.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author lio
 * @create 2024-09-04 20:27
 */
@Component
public class emailUtil {
    @Resource
    private JavaMailSender javaMailSender;
    @Value("${spring.mail.username}")
    private String from;
    @RabbitListener(queues = "email-Queue")
    public void sendMail(Message message) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper mineHelper = new MimeMessageHelper(mimeMessage, true);
            mineHelper.setFrom(from);
            mineHelper.setTo(message.getToEmail());
            mineHelper.setSubject("分配订单成功测试");
            mineHelper.setText("<html><body><h1>"+message.getMessage()+"</h1></body></html>", true);
            javaMailSender.send(mimeMessage);
        }catch (MessagingException e){
            e.printStackTrace();
        }
    }
}
