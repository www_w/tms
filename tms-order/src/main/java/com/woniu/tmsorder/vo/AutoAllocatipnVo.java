package com.woniu.tmsorder.vo;

import com.woniu.tmscommons.entity.Bill;
import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-08-29 9:20
 */
@Data
public class AutoAllocatipnVo {
    private Driver driver;
    private Vehicle vehicle;
    private BigDecimal unitPrice;
    private Integer billId;
}
