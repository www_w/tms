package com.woniu.tmsorder.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-08-29 15:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConditionVo {
    private Boolean isUrgent;
    // 是否冷藏订单，标识订单物品是否需要冷藏运输
    private Boolean isRefrigerated;
    // 保险价值，订单投保的金额
    private Boolean isInsurance;
    private Double totalWeight;//总重量
    private BigDecimal distance;//总距离
    private BigDecimal totalWorth;//总价值
    private String senderAddress;

}
