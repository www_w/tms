package com.woniu.tmsorder.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.woniu.tmscommons.entity.Bill;
import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.OrderItem;
import com.woniu.tmscommons.entity.SenderReceiver;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmscommons.entity.VehicleType;
import com.woniu.tmscommons.enums.OrderStatusEnum;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderVo {
    private Integer id;
    // 订单号，唯一标识一个订单
    private String orderNo;
    // 司机ID，关联司机信息
    private Integer driverId;
    // 车辆ID，关联车辆信息
    private Integer vehicleId;
    private Integer vehicleTypeId;
    private Integer billId;
    private BigDecimal totalWorth;
    // 订单状态，如待处理、运输中、已完成等
    private OrderStatusEnum status;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    // 订单创建时间，记录订单被创建的准确时间点
    private LocalDateTime createdAt;
    // 订单更新时间，记录订单信息最后一次被修改的时间点
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;
    // 订单总重量，以公斤为单位
    private BigDecimal totalWeight;
    // 是否紧急订单，标识订单是否需要优先处理
    private Boolean isUrgent;
    // 是否冷藏订单，标识订单物品是否需要冷藏运输
    private Boolean isRefrigerated;
    // 是否保价，订单投保的金额
    private Boolean isInsurance;
    // 运输距离，起运地到目的地的公里数
    private BigDecimal distance;


    // 关联对象
    private Driver driver;
    private Vehicle vehicle;
    private SenderReceiver senderReceiver;
    private Bill bill;
    private List<OrderItem> orderItems;
    private VehicleType vehicleType;
    //
}
