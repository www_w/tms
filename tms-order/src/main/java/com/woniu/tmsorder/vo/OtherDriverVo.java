package com.woniu.tmsorder.vo;

import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lio
 * @create 2024-09-02 11:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OtherDriverVo {
    private Integer driverId;
    private Integer vehicleId;
    private String orderNo;
    private Integer vehicleTypeId;
}
