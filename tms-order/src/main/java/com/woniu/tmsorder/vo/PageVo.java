package com.woniu.tmsorder.vo;

import com.woniu.tmscommons.entity.Order;
import lombok.Data;

/**
 * @author lio
 * @create 2024-09-05 15:11
 */
@Data
public class PageVo {
    private Integer pageNum;
    private Integer pageSize;
    private Order order;
}
