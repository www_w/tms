package com.woniu.tmsorder.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author lio
 * @create 2024-08-29 16:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PricingRuleVo {
    private Integer vehicle_type_id;
    private BigDecimal distance;
}
