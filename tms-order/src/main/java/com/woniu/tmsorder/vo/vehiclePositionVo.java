package com.woniu.tmsorder.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lio
 * @create 2024-09-06 15:15
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class vehiclePositionVo {
    private Integer vehicleId;
    private Double latitude;
    private Double longitude;
}
