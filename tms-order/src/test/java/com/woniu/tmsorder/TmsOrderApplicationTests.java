package com.woniu.tmsorder;

import com.woniu.tmsorder.config.RabbitConfig;
import com.woniu.tmsorder.pojo.Message;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class TmsOrderApplicationTests {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private RabbitTemplate rabbitTemplate;
    @Value("${spring.mail.username}")
    private String email;

    @Test
    void contextLoads() {
        String expiredKey = "order_info_order1725531312424";
        String s = expiredKey.split("_")[2];
        System.out.println(s);

    }

}
