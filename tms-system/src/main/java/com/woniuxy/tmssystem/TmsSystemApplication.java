package com.woniuxy.tmssystem;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = {"com.woniu","com.woniuxy.tmssystem","com.woniu.tmscommons.interceptor"})
@EnableDiscoveryClient
@MapperScan(basePackages = "com.woniuxy.tmssystem.mapper")
public class TmsSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmsSystemApplication.class, args);
    }

}
