package com.woniuxy.tmssystem.controller;

import com.woniu.tmscommons.entity.Menu;
import com.woniu.tmscommons.entity.RoleMenu;
import com.woniu.tmscommons.exception.system.MenuAddException;
import com.woniu.tmscommons.exception.system.MenuNotFoundException;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniuxy.tmssystem.service.MenuService;
import com.woniuxy.tmssystem.service.RoleMenuService;
import com.woniuxy.tmssystem.utils.MenuUtils;
import com.woniuxy.tmssystem.vo.MenuVo;
import com.woniuxy.tmssystem.vo.RoleMenuVo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.controller
 * @Project：TMS
 * @name：MenuController
 * @Date：2024/9/3 10:19
 * @Filename：MenuController
 * @Description:
 */
@RestController
@RequestMapping("sys/menu")
public class MenuController {
    @Resource
    private RoleMenuService roleMenuService;
    @Resource
    private MenuService menuService;

    //获取所有菜单
    @GetMapping("listAll")
    public ResponseResult<List<Menu>> listAllMenu() {
        List<Menu> menus = menuService.getAllMenu();
        if (menus.isEmpty()) {
            throw new MenuNotFoundException("菜单未找到");
        }
        return new ResponseResult<List<Menu>>().setCode(HttpStatus.OK.value()).setMessage("查询菜单成功").setData(menus);
    }

    //根据角色获取当前角色下所有菜单
    @GetMapping("list")
    public ResponseResult<List<RoleMenuVo>> getMenuList() {
        List<RoleMenuVo> roleMenus = roleMenuService.getRoleMenu();
        if (roleMenus.isEmpty()) {
            throw new MenuNotFoundException("菜单未找到");
        }
        return new ResponseResult<List<RoleMenuVo>>().setCode(HttpStatus.OK.value()).setMessage("查询菜单成功").setData(roleMenus);
    }

    //添加菜单
    @PostMapping("add")
    public ResponseResult<?> addMenu(@RequestBody MenuVo menuVo) {
        boolean suc = menuService.addMenu(menuVo);
        if (!suc) {
            throw new MenuAddException("添加菜单失败");
        }
        return new ResponseResult<>().setCode(200).setMessage("添加菜单成功");
    }

    //更新菜单
    @PostMapping("update")
    public ResponseResult<?> updateMenu(@RequestBody MenuVo menuVo) {
        boolean suc = menuService.updateMenu(menuVo);
        if (!suc) {
            throw new RuntimeException("更新菜单失败");
        }
        return new ResponseResult<>().setCode(200).setMessage("更新菜单成功");
    }

    //删除菜单
    @DeleteMapping("delete/{id}")
    public ResponseResult<?> deleteMenu(@PathVariable Integer id) {
        List<Menu> childMenu = menuService.getChildMenu(id);
        if (!childMenu.isEmpty()) {
            return new ResponseResult<>().setCode(201).setMessage("该菜单下有子菜单，请先删除子菜单");
        }
        boolean suc = menuService.deleteMenu(id);
        if (!suc) {
            throw new RuntimeException("删除菜单失败");
        }
        return new ResponseResult<>().setCode(200).setMessage("删除菜单成功");
    }

    //新增角色下菜单
    @PostMapping("addRoleMenu/{roleId}")
    public ResponseResult<?> addRoleMenu(@RequestBody List<Integer> menuIds, @PathVariable Integer roleId) {
       Map<Integer, Boolean> result = new HashMap<>();
        for (Integer menuId : menuIds) {
            RoleMenu roleMenu = roleMenuService.getRoleMenuByRoleIdAndMenuId(roleId, menuId);
            if (roleMenu == null) {
                boolean suc=roleMenuService.addRoleMenu(roleId, menuId);
                if (!suc){
                    result.put(menuId,false);
                }
            }else {
                result.put(menuId,true);
            }
        }
        if (result.isEmpty()) {
            return new ResponseResult<>().setCode(200).setMessage("新增角色菜单成功");
        }
        for (Map.Entry<Integer, Boolean> res : result.entrySet()) {
            if (res.getValue()){
                return new ResponseResult<>().setCode(201).setMessage("新增角色菜单:"+res.getKey()+"失败，角色下已有该菜单");
            }else {
                return new ResponseResult<>().setCode(500).setMessage("新增角色菜单:"+res.getKey()+"失败");
            }
        }
        return new ResponseResult<>();
    }

    //删除角色下菜单
    @GetMapping("deleteRoleMenu/{roleId}/{menuId}")
    public ResponseResult<?> deleteRoleMenu(@PathVariable Integer roleId, @PathVariable Integer menuId) {
        RoleMenuVo roleMenuVo = roleMenuService.getRoleMenuByRoleId(roleId);
        Menu menu = MenuUtils.getCurrentMenu(menuId, roleMenuVo);
        if (MenuUtils.hasChildren(menu)){
            return new ResponseResult<>().setCode(201).setMessage("该菜单下有子菜单，请先删除子菜单");
        }
        boolean suc = roleMenuService.deleteRoleMenuByRoleIdAndMenuId(roleId, menuId);
        if (!suc) {
            throw new RuntimeException("删除角色菜单失败");
        }
        return new ResponseResult<>().setCode(200).setMessage("删除角色菜单成功");
    }

}
