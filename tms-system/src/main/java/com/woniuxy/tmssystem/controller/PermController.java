package com.woniuxy.tmssystem.controller;

import com.woniu.tmscommons.entity.Perms;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniuxy.tmssystem.service.PermService;
import com.woniuxy.tmssystem.service.RolePermService;
import com.woniuxy.tmssystem.vo.PermVo;
import com.woniuxy.tmssystem.vo.RolePerm;
import com.woniuxy.tmssystem.vo.RolePermVo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.controller
 * @Project：TMS
 * @name：PermController
 * @Date：2024/8/26 11:10
 * @Filename：PermController
 * @Description:
 */
@RestController
@RequestMapping("sys/perm")
public class PermController {
    @Resource
    private PermService permService;

    @Resource
    private RolePermService rolePermService;
    @GetMapping("list")
    public ResponseResult<List<RolePermVo>> getPerms(){
        List<RolePermVo> perms = permService.listAllPermsByRole();
        if (perms == null){
            throw new RuntimeException("查询权限列表失败");
        }
        return new ResponseResult<List<RolePermVo>>().setCode(HttpStatus.OK.value()).setMessage("查询权限列表成功").setData(perms);
    }
    //删除某个角色权限
    @PostMapping("delete")
    public ResponseResult<Object> deletePerm(@RequestBody List<PermVo> deletePermVo){
        boolean suc = rolePermService.deletePermByRole(deletePermVo);
        if (!suc){
            throw new RuntimeException("删除权限失败");
        }
        return new ResponseResult<Object>().setCode(HttpStatus.OK.value()).setMessage("删除权限成功");
    }
    //查询所有权限
    @GetMapping("listAll")
    public ResponseResult<List<Perms>> listAllPerms(){
        List<Perms> perms = permService.listPerms();
        if (perms == null){
            throw new RuntimeException("查询权限列表失败");
        }
        return new ResponseResult<List<Perms>>().setCode(HttpStatus.OK.value()).setMessage("查询权限列表成功").setData(perms);
    }
    //获取还未拥有的权限
    @GetMapping("listNoPerm/{rid}")
    public ResponseResult<List<Perms>> listNoPerm(@PathVariable Integer rid){
        List<Perms> perms = permService.listNoPerms(rid);
        if (perms == null){
            throw new RuntimeException("查询权限列表失败");
        }
        return new ResponseResult<List<Perms>>().setCode(HttpStatus.OK.value()).setMessage("查询权限列表成功").setData(perms);
    }
    //添加角色权限
    @PostMapping("add")
    public ResponseResult<Object> addROlePerm(@RequestBody RolePerm rolePerm){
        boolean suc = permService.addPerm(rolePerm);
        if (!suc){
            throw new RuntimeException("添加权限失败");
        }
        return new ResponseResult<Object>().setCode(HttpStatus.OK.value()).setMessage("添加权限成功");
    }
    //更改权限
    @PostMapping("update")
    public ResponseResult<Object> updatePerm(@RequestBody Perms perms){
        boolean suc = permService.updateById(perms);
        if (!suc){
            throw new RuntimeException("更新权限失败");
        }
        return new ResponseResult<Object>().setCode(HttpStatus.OK.value()).setMessage("更新权限成功");
    }
    //添加权限
    @PostMapping("addPerm")
    public ResponseResult<Object> addPerm(@RequestBody Perms perms){
        boolean suc = permService.add(perms);
        if (!suc){
            throw new RuntimeException("添加权限失败");
        }
        return new ResponseResult<Object>().setCode(HttpStatus.OK.value()).setMessage("添加权限成功");
    }
    //删除权限
    @DeleteMapping("delete/{id}")
    public ResponseResult<Object> deletePerm(@PathVariable Integer id){
        boolean suc = permService.delete(id);
        if (!suc){
            throw new RuntimeException("删除权限失败");
        }
        return new ResponseResult<Object>().setCode(HttpStatus.OK.value()).setMessage("删除权限成功");
    }
}
