package com.woniuxy.tmssystem.controller;

import com.woniu.tmscommons.entity.Role;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniuxy.tmssystem.service.RoleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.controller
 * @Project：TMS
 * @name：RoleController
 * @Date：2024/8/29 11:56
 * @Filename：RoleController
 * @Description:
 */
@RestController
@RequestMapping("sys/role")
public class RoleController {
    @Resource
    private RoleService roleService;
    @GetMapping("list")
    public ResponseResult<List<Role>> listAllRoles(){
        List<Role> roles = roleService.listAllRoles();
        if (roles.isEmpty()){
            throw new RuntimeException("没有角色");
        }
        return new ResponseResult<List<Role>>().setCode(HttpStatus.OK.value()).setMessage("查询角色列表成功").setData(roles);
    }
}
