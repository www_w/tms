package com.woniuxy.tmssystem.controller;

import com.woniu.tmscommons.anno.RequirePerms;
import com.woniu.tmscommons.entity.User;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmscommons.utils.MinioUtil;
import com.woniuxy.tmssystem.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.controller
 * @Project：TMS
 * @name：UserController
 * @Date：2024/8/30 10:39
 * @Filename：UserController
 * @Description:
 */
@RestController
@RequestMapping("sys/user")
public class UserController {
    @Resource
    private UserService userService;
    //查询所有用户
    @GetMapping("list")
    @RequirePerms("user:select")
    public ResponseResult<List<User>> listAllUser(){
        List<User> users=userService.listAllUser();
        if (users.isEmpty()){
            throw new RuntimeException("没有用户");
        }
        return new ResponseResult<List<User>>().setCode(HttpStatus.OK.value()).setMessage("查询用户成功").setData(users);
    }
    //新增用户
    @PostMapping("add")
    public ResponseResult<Void> addUser(@RequestBody User user){
        boolean suc=userService.addUser(user);
        if (!suc){
            throw new RuntimeException("新增用户失败");
        }
        return new ResponseResult<Void>().setCode(HttpStatus.OK.value()).setMessage("新增用户成功");
    }
    //删除用户
    @DeleteMapping("delete/{uid}")
    public ResponseResult<Void> deleteUser(@PathVariable Integer uid){
        boolean suc=userService.deleteUser(uid);
        if (!suc){
            throw new RuntimeException("删除用户失败");
        }
        return new ResponseResult<Void>().setCode(HttpStatus.OK.value()).setMessage("删除用户成功");
    }
    //更新用户
    @PostMapping("update")
    public ResponseResult<Void> updateUser(@RequestBody User user){
        boolean suc=userService.updateUser(user);
        if (!suc){
            throw new RuntimeException("更新用户失败");
        }
        return new ResponseResult<Void>().setCode(HttpStatus.OK.value()).setMessage("更新用户成功");
    }
    //上传头像
    @PostMapping("uploadAvatar")
    public ResponseResult<String> upload(MultipartFile file) throws Exception {
        String fil = MinioUtil.upload(file);
        if (fil == null){
            throw new RuntimeException("上传失败");
        }
        return new ResponseResult<String>().setCode(HttpStatus.OK.value()).setMessage("上传成功").setData(fil);
    }
}
