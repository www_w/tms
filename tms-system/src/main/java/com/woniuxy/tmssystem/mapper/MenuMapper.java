package com.woniuxy.tmssystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.Menu;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.mapper
 * @Project：TMS
 * @name：MenuMapper
 * @Date：2024/9/3 16:58
 * @Filename：MenuMapper
 * @Description:
 */
public interface MenuMapper extends BaseMapper<Menu> {
}
