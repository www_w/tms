package com.woniuxy.tmssystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.Perms;
import com.woniuxy.tmssystem.vo.RolePermVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.mapper
 * @Project：TMS
 * @name：PermMapper
 * @Date：2024/8/26 11:11
 * @Filename：PermMapper
 * @Description:
 */
public interface PermMapper extends BaseMapper<Perms> {
    // 查询某个角色所有权限
    List<RolePermVo> listAllPermsByRole();
    //删除某个角色的某个权限
    int deletePermByRole(@Param("rid") int rid,@Param("pid") int pid);

    List<Perms> listNoPerms(Integer rid);
}
