package com.woniuxy.tmssystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.Role;
import com.woniuxy.tmssystem.vo.RoleVo;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.mapper
 * @Project：TMS
 * @name：RoleMapper
 * @Date：2024/8/29 11:50
 * @Filename：RoleMapper
 * @Description:
 */
public interface RoleMapper extends BaseMapper<Role> {
}
