package com.woniuxy.tmssystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.RoleMenu;
import com.woniuxy.tmssystem.vo.RoleMenuVo;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.mapper
 * @Project：TMS
 * @name：RoleMenuMapper
 * @Date：2024/9/3 9:59
 * @Filename：RoleMenuMapper
 * @Description:
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    List<RoleMenuVo> getRoleMenuVo();

    RoleMenuVo getRoleMenuVoByRoleId(Integer roleId);
}
