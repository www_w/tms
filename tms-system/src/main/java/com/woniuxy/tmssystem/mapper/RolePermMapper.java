package com.woniuxy.tmssystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.tmssystem.vo.RolePerm;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.mapper
 * @Project：TMS
 * @name：RolePermMapper
 * @Date：2024/8/28 17:11
 * @Filename：RolePermMapper
 * @Description:
 */
public interface RolePermMapper extends BaseMapper<RolePerm> {
}
