package com.woniuxy.tmssystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.User;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.mapper
 * @Project：TMS
 * @name：UserMapper
 * @Date：2024/8/30 10:38
 * @Filename：UserMapper
 * @Description:
 */
public interface UserMapper extends BaseMapper<User> {
    List<User> findAll();
}
