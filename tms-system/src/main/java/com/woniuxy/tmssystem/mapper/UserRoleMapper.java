package com.woniuxy.tmssystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.UserRole;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.mapper
 * @Project：TMS
 * @name：UserRoleMapper
 * @Date：2024/9/2 15:06
 * @Filename：UserRoleMapper
 * @Description:
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {
    boolean updateUserRole(UserRole userRole);
}
