package com.woniuxy.tmssystem.service;

import com.woniu.tmscommons.entity.Menu;
import com.woniuxy.tmssystem.vo.MenuVo;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service
 * @Project：TMS
 * @name：MenuService
 * @Date：2024/9/3 16:58
 * @Filename：MenuService
 * @Description:
 */
public interface MenuService {
    boolean addMenu(MenuVo menuVo);
    List<Menu> getAllMenu();

    boolean updateMenu(MenuVo menuVo);

    boolean deleteMenu(Integer id);

    List<Menu> getChildMenu(Integer pid);
}
