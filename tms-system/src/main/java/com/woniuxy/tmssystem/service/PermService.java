package com.woniuxy.tmssystem.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.tmscommons.entity.Perms;
import com.woniuxy.tmssystem.vo.RolePerm;
import com.woniuxy.tmssystem.vo.RolePermVo;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service
 * @Project：TMS
 * @name：PermService
 * @Date：2024/8/26 11:11
 * @Filename：PermService
 * @Description:
 */
public interface PermService{
    List<RolePermVo> listAllPermsByRole();

    List<Perms> listPerms();

    List<Perms> listNoPerms(Integer rid);

    boolean addPerm(RolePerm rolePerm);


    boolean updateById(Perms perms);

    boolean add(Perms perms);

    boolean delete(Integer id);
}
