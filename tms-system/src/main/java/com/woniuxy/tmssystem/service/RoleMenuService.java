package com.woniuxy.tmssystem.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.tmscommons.entity.RoleMenu;
import com.woniuxy.tmssystem.vo.RoleMenuVo;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service
 * @Project：TMS
 * @name：RoleMenuService
 * @Date：2024/9/3 10:16
 * @Filename：RoleMenuService
 * @Description:
 */
public interface RoleMenuService{
    List<RoleMenuVo> getRoleMenu();

    RoleMenuVo getRoleMenuByRoleId(Integer roleId);

    boolean addRoleMenu(Integer roleId, List<Integer> menuIds);
    boolean addRoleMenu(Integer roleId, Integer menuId);

    RoleMenu getRoleMenuByRoleIdAndMenuId(Integer roleId,Integer menuId);

    boolean deleteRoleMenuByRoleIdAndMenuId(Integer roleId,Integer menuId);

}
