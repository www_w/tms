package com.woniuxy.tmssystem.service;

import com.woniu.tmscommons.entity.Perms;
import com.woniuxy.tmssystem.vo.PermVo;
import com.woniuxy.tmssystem.vo.RolePerm;
import com.woniuxy.tmssystem.vo.RolePermVo;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service
 * @Project：TMS
 * @name：RolePermService
 * @Date：2024/8/28 17:11
 * @Filename：RolePermService
 * @Description:
 */
public interface RolePermService {
    boolean deletePermByRole(List<PermVo> rolePerms);
}
