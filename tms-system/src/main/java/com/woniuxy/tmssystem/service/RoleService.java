package com.woniuxy.tmssystem.service;

import com.woniu.tmscommons.entity.Role;
import com.woniuxy.tmssystem.vo.RoleVo;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service
 * @Project：TMS
 * @name：RoleService
 * @Date：2024/8/29 11:51
 * @Filename：RoleService
 * @Description:
 */

public interface RoleService {
    List<Role> listAllRoles();
}
