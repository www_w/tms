package com.woniuxy.tmssystem.service;

import com.woniu.tmscommons.entity.User;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service
 * @Project：TMS
 * @name：UserService
 * @Date：2024/8/30 10:38
 * @Filename：UserService
 * @Description:
 */
public interface UserService {
    List<User> listAllUser();

    boolean addUser(User user);

    boolean deleteUser(Integer uid);

    boolean updateUser(User user);
}
