package com.woniuxy.tmssystem.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.tmscommons.entity.Perms;
import com.woniuxy.tmssystem.mapper.PermMapper;
import com.woniuxy.tmssystem.mapper.RolePermMapper;
import com.woniuxy.tmssystem.service.PermService;
import com.woniuxy.tmssystem.vo.RolePerm;
import com.woniuxy.tmssystem.vo.RolePermVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service.impl
 * @Project：TMS
 * @name：PermServiceImpl
 * @Date：2024/8/26 11:11
 * @Filename：PermServiceImpl
 * @Description:
 */
@Service
public class PermServiceImpl implements PermService {
    @Resource
    private RolePermMapper rolePermMapper;
    @Resource
    private PermMapper permMapper;

    /**
     * 根据角色查询所有权限列表
     * @return
     */
    @Override
    public List<RolePermVo> listAllPermsByRole() {
        return permMapper.listAllPermsByRole();
    }
    @Override
    public List<Perms> listPerms() {
        LambdaQueryWrapper<Perms> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.orderByAsc(Perms::getId);
        return permMapper.selectList(lambdaQueryWrapper);
    }

    @Override
    public List<Perms> listNoPerms(Integer rid) {
        return permMapper.listNoPerms(rid);
    }

    @Override
    public boolean addPerm(RolePerm rolePerm) {
        return rolePermMapper.insert(rolePerm) > 0;
    }

    @Override
    public boolean updateById(Perms perms) {
        System.out.println(perms);
        return permMapper.updateById(perms)>0;
    }

    @Override
    public boolean add(Perms perms) {
        return permMapper.insert(perms)>0;
    }

    @Override
    public boolean delete(Integer id) {
        LambdaQueryWrapper<Perms> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Perms::getId,id);
        return permMapper.delete(lambdaQueryWrapper)>0;
    }
}
