package com.woniuxy.tmssystem.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.woniuxy.tmssystem.mapper.PermMapper;
import com.woniuxy.tmssystem.mapper.RolePermMapper;
import com.woniuxy.tmssystem.service.RolePermService;
import com.woniuxy.tmssystem.vo.PermVo;
import com.woniuxy.tmssystem.vo.RolePerm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service.impl
 * @Project：TMS
 * @name：RolePermServiceImpl
 * @Date：2024/8/28 17:12
 * @Filename：RolePermServiceImpl
 * @Description:
 */
@Service
public class RolePermServiceImpl implements RolePermService {
    @Resource
    private RolePermMapper rolePermMapper;

    @Override
    @Transactional
    public boolean deletePermByRole(List<PermVo> permVos) {
        boolean flag=true;
        for (PermVo permVo : permVos) {
            LambdaQueryWrapper<RolePerm> queryWrapper=new LambdaQueryWrapper<>();
            //如果满足rid和pid都相等就删除，使用LambdaQueryWrapper
            queryWrapper
                    .eq(RolePerm::getRoleId,permVo.getRid())
                    .eq(RolePerm::getPermId,permVo.getPid());
            int delete = rolePermMapper.delete(queryWrapper);
            flag=(delete>0);
        }
        return flag;
    }
}
