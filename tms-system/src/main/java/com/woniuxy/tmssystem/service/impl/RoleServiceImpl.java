package com.woniuxy.tmssystem.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.woniu.tmscommons.entity.Role;
import com.woniuxy.tmssystem.mapper.RoleMapper;
import com.woniuxy.tmssystem.service.RoleService;
import com.woniuxy.tmssystem.vo.RoleVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service.impl
 * @Project：TMS
 * @name：RoleServiceImpl
 * @Date：2024/8/29 11:52
 * @Filename：RoleServiceImpl
 * @Description:
 */

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleMapper roleMapper;
    @Override
    public List<Role> listAllRoles() {
        LambdaQueryWrapper<Role> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.orderByAsc(Role::getId);
        return roleMapper.selectList(lambdaQueryWrapper);
    }
}
