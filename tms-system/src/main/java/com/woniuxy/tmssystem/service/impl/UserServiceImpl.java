package com.woniuxy.tmssystem.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.woniu.tmscommons.entity.User;
import com.woniu.tmscommons.entity.UserRole;
import com.woniuxy.tmssystem.mapper.RoleMapper;
import com.woniuxy.tmssystem.mapper.UserMapper;
import com.woniuxy.tmssystem.mapper.UserRoleMapper;
import com.woniuxy.tmssystem.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.service.impl
 * @Project：TMS
 * @name：UserServiceImpl
 * @Date：2024/8/30 10:39
 * @Filename：UserServiceImpl
 * @Description:
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private UserRoleMapper userRoleMapper;
    @Resource
    private UserMapper userMapper;
    @Override
    public List<User> listAllUser() {
        return userMapper.findAll();
    }

    @Override
    @Transactional
    public boolean addUser(User user) {
        user.setId(null);
        int insert = userMapper.insert(user);
        if (insert>0){
            return userRoleMapper.insert(new UserRole().setUserId(user.getId()).setRoleId(user.getRoleId()))>0;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean deleteUser(Integer uid) {
        int deleteById = userMapper.deleteById(uid);
        if (deleteById>0){
            return userRoleMapper.delete(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId,uid))>0;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean updateUser(User user) {
        int updateById = userMapper.updateById(user);
        if (updateById>0){
            return userRoleMapper.updateUserRole(new UserRole().setUserId(user.getId()).setRoleId(user.getRoleId()));
        }
        return false;
    }
}
