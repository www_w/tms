package com.woniuxy.tmssystem.utils;

import com.woniu.tmscommons.entity.Menu;
import com.woniuxy.tmssystem.vo.RoleMenuVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.utils
 * @Project：TMS
 * @name：MenuUtils
 * @Date：2024/9/5 16:07
 * @Filename：MenuUtils
 * @Description:
 */
public class MenuUtils {
    //构建角色菜单树
    public static List<RoleMenuVo> buildTree(List<RoleMenuVo> roleMenuVos) {
        for (RoleMenuVo roleMenuVo : roleMenuVos) {
            List<Menu> menus = roleMenuVo.getMenus();
            List<Menu> newMenus = buildMenuTree(menus);
            roleMenuVo.setMenus(newMenus);
        }
        return roleMenuVos;
    }
    //构建菜单树
    public static List<Menu> buildMenuTree(List<Menu> menus) {
        Map<Integer, Menu> menuMap = new HashMap<>();
        List<Menu> rootMenus = new ArrayList<>();
        // 将所有菜单项放入 map 中，便于快速查找
        for (Menu menu : menus) {
            menuMap.put(menu.getId(), menu);
        }
        // 遍历所有菜单项，构建树形结构
        for (Menu menu : menus) {
            if (menu.getPid() == null || menu.getPid() == 0) {
                // 如果是根节点，则直接加入根菜单列表
                rootMenus.add(menu);
            } else {
                // 如果不是根节点，则找到其父节点，并添加为子节点
                Menu parent = menuMap.get(menu.getPid());
                if (parent != null) {
                    // 确保父菜单的 children 集合已经初始化
                    if (parent.getChildren() == null) {
                        parent.setChildren(new ArrayList<>()); // 初始化 children 集合
                    }
                    parent.getChildren().add(menu);
                }
            }
        }
        return rootMenus;
    }
    //递归查找当前menus是否有children
    public static boolean hasChildren(Menu menu) {
        // 确保 menu 不为 null
        if (menu == null) {
            return false;
        }
        // 检查当前节点是否有子菜单
        if (menu.getChildren() != null && !menu.getChildren().isEmpty()) {
            return true;
        }
        if (menu.getChildren()!=null){
            // 递归检查所有子节点
            for (Menu child : menu.getChildren()) {
                if (hasChildren(child)) {
                    return true;
                }
            }
        }
        return false;
    }
    //根据菜单id得到当前选择的菜单
    public static Menu getCurrentMenu(Integer menuId,RoleMenuVo roleMenuVo) {
        List<Menu> menus = roleMenuVo.getMenus().stream().filter(menu -> menu.getId().equals(menuId)).collect(Collectors.toList());
        return menus.get(0);
    }
}
