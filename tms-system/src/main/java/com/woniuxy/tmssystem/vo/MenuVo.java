package com.woniuxy.tmssystem.vo;

import com.woniu.tmscommons.entity.Menu;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.vo
 * @Project：TMS
 * @name：MenuVo
 * @Date：2024/9/3 17:06
 * @Filename：MenuVo
 * @Description:
 */
public class MenuVo extends Menu {
}
