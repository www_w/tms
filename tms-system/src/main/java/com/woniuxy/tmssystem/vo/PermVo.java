package com.woniuxy.tmssystem.vo;

import com.woniu.tmscommons.entity.Perms;
import lombok.Data;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.vo
 * @Project：TMS
 * @name：PermVo
 * @Date：2024/8/28 9:59
 * @Filename：PermVo
 * @Description:
 */
@Data
public class PermVo extends Perms {
    private Integer rid;
    private Integer pid;
}
