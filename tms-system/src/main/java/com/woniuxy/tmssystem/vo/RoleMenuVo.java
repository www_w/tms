package com.woniuxy.tmssystem.vo;

import com.woniu.tmscommons.entity.Menu;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.vo
 * @Project：TMS
 * @name：RoleMenuVo
 * @Date：2024/9/3 10:01
 * @Filename：RoleMenuVo
 * @Description:
 */
@Data
@Accessors(chain = true)
public class RoleMenuVo {
    private Integer roleId;
    private String  roleName;
    private List<Menu> menus;
}
