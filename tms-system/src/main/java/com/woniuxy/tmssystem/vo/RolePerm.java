package com.woniuxy.tmssystem.vo;

import lombok.Data;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.vo
 * @Project：TMS
 * @name：DeletePermVo
 * @Date：2024/8/28 15:52
 * @Filename：DeletePermVo
 * @Description:
 */
@Data
public class RolePerm {
    private Integer roleId;
    private Integer permId;
}
