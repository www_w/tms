package com.woniuxy.tmssystem.vo;


import com.woniu.tmscommons.entity.Perms;
import com.woniu.tmscommons.entity.Role;
import lombok.Data;

import java.util.List;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.vo
 * @Project：TMS
 * @name：PermVo
 * @Date：2024/8/26 11:30
 * @Filename：PermVo
 * @Description:
 */
@Data
public class RolePermVo extends Role {
    private Integer rid;
    private List<PermVo> permList;
}
