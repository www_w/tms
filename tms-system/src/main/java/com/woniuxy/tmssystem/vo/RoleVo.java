package com.woniuxy.tmssystem.vo;

import com.woniu.tmscommons.entity.Role;
import lombok.Data;

/**
 * @Author：田江涛
 * @Package：com.woniuxy.tmssystem.vo
 * @Project：TMS
 * @name：RoleVo
 * @Date：2024/8/29 11:50
 * @Filename：RoleVo
 * @Description:
 */
public class RoleVo extends Role {
}
