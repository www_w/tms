package com.woniu.tmsvehicle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = {"com.woniu.tmsvehicle"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.woniu.tmsvehicle","com.woniu.tmscommons.feign"})
public class TmsVehicleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmsVehicleApplication.class, args);
    }
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
