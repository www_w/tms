package com.woniu.tmsvehicle.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MailRabbitConfig {
    public static final String MAIL_QUEUE_NAME = "mail_queue";
    public static final String MAIL_DIRECT_EXCHANGE_NAME = "mail_direct_exchange";
    public static final String MAIL_ROUTING_KEY = "mail_route";


//    public static final String MAIL_DEAD_ROUTING_KEY = "mail_dead_route";

    @Bean
    public Queue mailQueue() {
        Map<String, Object> args = new HashMap<>();
        // 设置死信交换机
        args.put("x-dead-letter-exchange", RabbitConfig.DEAD_EXCHANGE_NAME);
        // 设置死信路由
        args.put("x-dead-letter-routing-key", RabbitConfig.DEAD_ROUTING_KEY);
        // 超时时间
        args.put("x-message-ttl", RabbitConfig.TTL);
        // 设置队列长度
        args.put("x-max-length", RabbitConfig.MAX_LENGTH);
        // 设置溢出处理方式
        args.put("x-overflow", RabbitConfig.OVERFLOW_MODE);
        return new Queue(MAIL_QUEUE_NAME, true, false, false, args);
    }

    @Bean
    public DirectExchange mailDirectExchange(){
        return new DirectExchange(MAIL_DIRECT_EXCHANGE_NAME);
    }
    @Bean
    public Binding bindingMailQueueToMailDirectExchange(){
        return BindingBuilder.bind(mailQueue()).to(mailDirectExchange()).with(MAIL_ROUTING_KEY);
    }

//    @Bean
//    public Queue mailDeadQueue(){
//        return new Queue(MAIL_DEAD_QUEUE_NAME);
//    }
//    @Bean
//    public DirectExchange mailDeadDirectExchange(){
//        return new DirectExchange(MAIL_DEAD_EXCHANGE_NAME);
//    }
//    @Bean
//    public Binding bindingMailDeadQueueToDeadDirectExchange(){
//        return BindingBuilder.bind(mailDeadQueue()).to(mailDeadDirectExchange()).with(MAIL_DEAD_ROUTING_KEY);
//    }

}
