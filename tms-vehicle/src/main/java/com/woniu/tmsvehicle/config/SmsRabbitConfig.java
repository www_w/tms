package com.woniu.tmsvehicle.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SmsRabbitConfig {
    public static final String SMS_QUEUE_NAME = "sms_queue";
    public static final String SMS_DIRECT_EXCHANGE_NAME = "sms_direct_exchange";
    public static final String SMS_ROUTING_KEY = "sms_route";

    @Bean
    public Queue smsQueue() {
        Map<String, Object> args = new HashMap<>();
        // 设置死信交换机
        args.put("x-dead-letter-exchange", RabbitConfig.DEAD_EXCHANGE_NAME);
        // 设置死信路由
        args.put("x-dead-letter-routing-key", RabbitConfig.DEAD_ROUTING_KEY);
        // 超时时间
        args.put("x-message-ttl", RabbitConfig.TTL);
        // 设置队列长度
        args.put("x-max-length", RabbitConfig.MAX_LENGTH);
        // 设置溢出处理方式
        args.put("x-overflow", RabbitConfig.OVERFLOW_MODE);
        return new Queue(SMS_QUEUE_NAME, true, false, false, args);
    }
    @Bean
    public DirectExchange smsDirectExchange(){
        return new DirectExchange(SMS_DIRECT_EXCHANGE_NAME);
    }
    @Bean
    public Binding bindingSmsQueueToSmsDirectExchange(){
        return BindingBuilder.bind(smsQueue()).to(smsDirectExchange()).with(SMS_ROUTING_KEY);
    }
}
