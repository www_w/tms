package com.woniu.tmsvehicle.consumer;

import com.rabbitmq.client.Channel;
import com.woniu.tmsvehicle.config.MailRabbitConfig;
import com.woniu.tmsvehicle.config.RabbitConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class DeadConsumer {
    @RabbitListener(queues = RabbitConfig.DEAD_QUEUE_NAME)
    public void refreshConsumer(String msg, Channel channel, Message message){
        System.out.println("dead queue msg:"+msg);
        try {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
