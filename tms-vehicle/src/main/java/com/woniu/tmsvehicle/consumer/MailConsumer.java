package com.woniu.tmsvehicle.consumer;

import com.rabbitmq.client.Channel;
import com.woniu.tmsvehicle.config.MailRabbitConfig;
import com.woniu.tmsvehicle.config.RabbitConfig;
import com.woniu.tmsvehicle.utils.MailUtil;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;


@Component
public class MailConsumer {
    @Resource
    private MailUtil mailUtil;

    @RabbitListener(queues = MailRabbitConfig.MAIL_QUEUE_NAME)
    public void receiveMail(String msg, Channel channel, Message message){
        System.out.println("邮件消费者接收消息："+msg);
        try {
            channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String emailAddr = msg.split("-")[1];
        mailUtil.sendEmail(emailAddr);
    }
}
