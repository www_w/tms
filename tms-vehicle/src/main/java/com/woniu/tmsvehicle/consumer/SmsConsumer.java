package com.woniu.tmsvehicle.consumer;

import com.rabbitmq.client.Channel;
import com.woniu.tmsvehicle.config.MailRabbitConfig;
import com.woniu.tmsvehicle.config.SmsRabbitConfig;
import com.woniu.tmsvehicle.utils.MailUtil;
import com.woniu.tmsvehicle.utils.SmsUtil;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;


@Component
public class SmsConsumer {
    @Resource
    private SmsUtil smsUtil;

    @RabbitListener(queues = SmsRabbitConfig.SMS_QUEUE_NAME)
    public void receiveMail(String msg, Channel channel, Message message){
        System.out.println("短信消费者接收消息："+msg);
        try {
            channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String phoneNum = msg.split("-")[1];
        smsUtil.sendSms(phoneNum);
    }
}
