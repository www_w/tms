package com.woniu.tmsvehicle.controller;

import com.woniu.tmscommons.anno.RequirePerms;
import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsvehicle.service.DriverService;
import com.woniu.tmsvehicle.vo.DriverVo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/vehicle/driver")
public class DriverController {
    @Resource
    private DriverService driverService;

    @PostMapping("/findAll")
    @RequirePerms("driver:findAll")
    public ResponseResult<List<Driver>> findAll(){
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", driverService.findAll());
    }

    @GetMapping("/deleteById/{id}")
    public ResponseResult<Object> deleteById(@PathVariable("id") Integer id){
        if(driverService.deleteById(id) > 0){
            return new ResponseResult<>(HttpStatus.OK.value(), "删除成功", null);
        }
        return new ResponseResult<>(HttpStatus.MULTI_STATUS.value(), "删除失败", null);
    }

    @GetMapping("/selectById/{id}")
    public ResponseResult<Object> selectById(@PathVariable("id") Integer id){
        Driver driver = driverService.selectById(id);
        if(driver != null ){
            return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", driver);
        }
        return new ResponseResult<>(HttpStatus.MULTI_STATUS.value(), "查询失败", null);
    }

    @PostMapping("/insertAMsg")
    public ResponseResult<Object> insertAMsg(@RequestBody DriverVo driverVo){
        int num = driverService.insertAMsg(driverVo.getDriver());
        if(num > 0){
            return new ResponseResult<>(HttpStatus.OK.value(), "添加成功", null);
        }
        return new ResponseResult<>(HttpStatus.MULTI_STATUS.value(), "添加失败", null);
    }

    @PostMapping("/updateAMsg")
    public ResponseResult<Object> updateAMsg(@RequestBody DriverVo driverVo){
        int num = driverService.updateAMsg(driverVo.getDriver());
        if(num > 0){
            return new ResponseResult<>(HttpStatus.OK.value(), "修改成功", null);
        }
        return new ResponseResult<>(HttpStatus.MULTI_STATUS.value(), "修改成功", null);
    }


    /**
     * 查询空闲司机
     **/
    @PostMapping("/findAvailableDriver")
    public ResponseResult<List<Driver>> findAvailableDriver(){
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", driverService.findAvailableDriver());
    }

    /**
     *多条件查询
     * */
    @PostMapping("/findByCondition")
    public ResponseResult<List<Driver>> findByCondition(@RequestBody Driver driver){
        List<Driver> drivers = driverService.findByCondition(driver);
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", drivers);
    }

    @GetMapping("fixTheDriveStatus/{id}")
    public ResponseResult<Object> fixTheDriveStatus(@PathVariable("id") Integer id){
        int num = driverService.fixTheDriveStatusForMyself(id);
        return new ResponseResult<>(HttpStatus.OK.value(), "修改司机状态成功", null);
    }
}
