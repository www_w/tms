package com.woniu.tmsvehicle.controller;

import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.PricingRule;
import com.woniu.tmscommons.entity.VehicleType;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsvehicle.service.PricingRuleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/vehicle/pricing")
public class PricingRuleController {
    @Resource
    private PricingRuleService pricingRuleService;


    @PostMapping("/findAll")
    public ResponseResult<List<PricingRule>> findAll(){
        List<PricingRule> rules = pricingRuleService.findAll();
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", rules);
    }

    /**
     * 修改单价
     * */
    @PostMapping("/updateAMsg")
    public ResponseResult<Object> updateThePrice(@RequestBody PricingRule pricingRule){
        int num = pricingRuleService.updateThePrice(pricingRule);
        return new ResponseResult<>(HttpStatus.OK.value(), "价格修改成功" ,null);
    }

    /**
     * 查询车辆类型信息
     * */
    @GetMapping("/findDetailOfType/{vehicleTypeId}")
    public ResponseResult<VehicleType> findDetailOfType(@PathVariable("vehicleTypeId") Integer vehicleTypeId){
        VehicleType vehicleType = pricingRuleService.findDetailOfType(vehicleTypeId);
        return new ResponseResult<>(HttpStatus.OK.value(), "车辆类型查询成功",vehicleType);
    }
}
