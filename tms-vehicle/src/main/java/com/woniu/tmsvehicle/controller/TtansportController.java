package com.woniu.tmsvehicle.controller;

import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsvehicle.service.TransportService;
import com.woniu.tmsvehicle.vo.AddrVo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/vehicle/transport")
public class TtansportController {
    @Resource
    private TransportService transportService;

    /**
     * 查询当前处于空闲状态的司机和车辆
     * */
    @GetMapping("/findAvailableDriverAndVehicle")
    public ResponseResult<Object> findAvailableDriverAndVehicle(){
        List<Driver> drivers = transportService.findAvailableDriverAndVehicle();
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", drivers);
    }

    /**
     *
     * 根据ip地址来找到经纬度
     * */
    @GetMapping("/findNearestDriverAndVehicle/{ip}")
    public ResponseResult<Object> findCurrentLatAndLng(@PathVariable("ip") String ip){
        Map<String,String> map = transportService.findNearestDriverAndVehicle(ip);
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", map);
    }

    /**
     * 查询两点之间的距离,单位m
     * */
    @GetMapping("/findDistance/{latFrom}/{lngFrom}/{latTo}/{lngTo}")
    public ResponseResult<Object> findDistance(@PathVariable("latFrom") String latFrom,
                                               @PathVariable("lngFrom") String lngFrom,
                                               @PathVariable("latTo") String latTo,
                                               @PathVariable("lngTo") String lngTo){
        AddrVo addrVo = new AddrVo();
        addrVo.setLatFrom(latFrom);
        addrVo.setLngFrom(lngFrom);
        addrVo.setLatTo(latTo);
        addrVo.setLngTo(lngTo);
        int distance = transportService.findDistance(addrVo);
        Double dd = Double.valueOf(distance/1000);
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", dd);
    }

    /**
     * 查询离当前目的地最近的司机和车辆
     * */
//    @GetMapping("/findShortestDistance/{latTo}/{lngTo}")
//    public ResponseResult<Driver> findShortestDistance(@PathVariable("latTo") String latTo,
//                                                       @PathVariable("lngTo") String lngTo){
//        Driver driver = transportService.findShortestDistance(latTo,lngTo);
//        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功",driver );
//    }

    /**
     * 传一个吨位的数字，匹配一个最合适的车辆信息返还
     * */
    @GetMapping("/findRightVehicle/{capacity}/{senderAddress}")
    public ResponseResult<Vehicle> findRightVehicle(@PathVariable("capacity") Double capacity,@PathVariable("senderAddress") String senderAddress){
        Vehicle rightVehicle = transportService.findRightVehicle(capacity, senderAddress);
        if(rightVehicle != null){
            return new ResponseResult<>(HttpStatus.OK.value(), "查询成功",rightVehicle);
        }
        return new ResponseResult<>(HttpStatus.MULTI_STATUS.value(), "没有匹配的车辆",null);
    }

    /**
     * 根据经纬度查询准确地址
     * */
    @GetMapping("/getTheRightAddr/{lat}/{lng}")
    public ResponseResult<String> getTheRightAddr(@PathVariable("lat") String lat,@PathVariable("lng") String lng){
        String addr = transportService.getTheRightAddr(lat,lng);
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功",addr );
    }

    /**
     * 发送邮件接口
     * */
    @GetMapping("/sendEmail/{email}")
    public ResponseResult<Object> sendEmail(@PathVariable("email") String email){
        transportService.sendEmail(email);
        return new ResponseResult<>(HttpStatus.OK.value(),"发送成功",null);
    }

    /**
     * 发送短信的接口
     * */
    @GetMapping("/sendSms/{phoneNum}")
    public ResponseResult<Object> sendSms(@PathVariable("phoneNum") String phoneNum){
        transportService.sendSms(phoneNum);
        return  new ResponseResult<>(HttpStatus.OK.value(),"短信发送成功",null);
    }
}
