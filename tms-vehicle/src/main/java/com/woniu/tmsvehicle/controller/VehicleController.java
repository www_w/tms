package com.woniu.tmsvehicle.controller;


import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmscommons.response.ResponseResult;
import com.woniu.tmsvehicle.service.VehicleService;
import com.woniu.tmsvehicle.vo.UpdateStatusVo;
import com.woniu.tmsvehicle.vo.VehicleVo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/vehicle/vehicle")
public class VehicleController {

    @Resource
    private VehicleService vehicleService;

    @PostMapping("/findAll")
    public ResponseResult<List<Vehicle>> findAll(){
        List<Vehicle> vehicles = vehicleService.findAll();
        return new ResponseResult<>(HttpStatus.OK.value(),"查询成功",vehicles);
    }

    @GetMapping("/selectById/{id}")
    public ResponseResult<Vehicle> selectById(@PathVariable("id") Integer id){
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", vehicleService.selectById(id));
    }

    @GetMapping("/deleteById/{id}")
    public ResponseResult<Object> deleteById(@PathVariable("id") Integer id){
        int num = vehicleService.deleteById(id);
        return new ResponseResult<>(HttpStatus.OK.value(), "删除成功", null);
    }

    @PostMapping("/insertOne")
    public ResponseResult<Object> insertOne(@RequestBody Vehicle vehicle){
        int num = vehicleService.insertOne(vehicle);
        if(num > 0){
            return new ResponseResult<>(HttpStatus.OK.value(), "添加成功", null);
        }
        return new ResponseResult<>(HttpStatus.MULTI_STATUS.value(), "添加失败", null);
    }


    @PostMapping("/updateAMsg")
    public ResponseResult<Object> updateAMsg(@RequestBody VehicleVo vehicleVo){
        int num = vehicleService.updateAMsg(vehicleVo.getVehicle());
        if(num > 0){
            return new ResponseResult<>(HttpStatus.OK.value(), "修改成功", null);
        }
        return new ResponseResult<>(HttpStatus.MULTI_STATUS.value(), "修改成功", null);
    }


    /**
     *多条件查询
     * */
    @PostMapping("/findByCondition")
    public ResponseResult<List<Vehicle>> findByCondition(@RequestBody Vehicle vehicle){
        List<Vehicle> vehicles = vehicleService.findByCondition(vehicle);
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", vehicles);
    }

    /**
     * 根据司机的用户id获取司机与车辆的信息，包括车辆类型信息
     * */
    @GetMapping("/findDriverAndVehicle/{userId}")
    public ResponseResult<Vehicle> findDriverAndVehicle(@PathVariable("userId") Integer userId){
        Vehicle vehicle = vehicleService.findVehicleByUserId(userId);
        return new ResponseResult<>(HttpStatus.OK.value(), "查询成功", vehicle);
    }

    /**
     * 修改当前车辆的状态
     * */
    @GetMapping("/updateStatus/{id}")
    public ResponseResult<Object> updateStatus(@PathVariable("id") Integer id){
        int num  = vehicleService.updateStatus(id);
        return new ResponseResult<>(HttpStatus.OK.value(), "车辆状态修改成功",null);
    }


    /**
     * 根据订单修改车辆状态和司机状态
     * */
    @PostMapping ("/updateVehicleAndDriverStatus")
    public ResponseResult<Object> updateVehicleAndDriverStatus(@RequestBody UpdateStatusVo updateStatusVo){
        vehicleService.updateVehicleAndDriverStatus(updateStatusVo);
        return new ResponseResult<>(HttpStatus.OK.value(), "车辆状态修改成功",null);
    }

}
