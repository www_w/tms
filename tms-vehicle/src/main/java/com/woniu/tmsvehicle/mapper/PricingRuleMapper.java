package com.woniu.tmsvehicle.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.PricingRule;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PricingRuleMapper extends BaseMapper<PricingRule> {
}
