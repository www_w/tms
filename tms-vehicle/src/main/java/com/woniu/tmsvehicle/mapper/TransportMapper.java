package com.woniu.tmsvehicle.mapper;


import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TransportMapper{
    List<Driver> findAvailableDriverAndVehicle();

    List<Vehicle> findRightVehicle(@Param("capacity") Double capacity);
}
