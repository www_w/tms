package com.woniu.tmsvehicle.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.Vehicle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface VehicleMapper extends BaseMapper<Vehicle> {

    List<Vehicle> findAll();

    List<Vehicle> findByCondition(Vehicle vehicle);

    Vehicle findVehicleByUserId(Integer userId);

    List<Vehicle> findOurVehicle();
}
