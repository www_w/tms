package com.woniu.tmsvehicle.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.tmscommons.entity.VehicleType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VehicleTypeMapper extends BaseMapper<VehicleType> {
}
