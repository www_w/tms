package com.woniu.tmsvehicle.producer;

import com.woniu.tmsvehicle.config.MailRabbitConfig;
import com.woniu.tmsvehicle.config.RabbitConfig;
import com.woniu.tmsvehicle.config.SmsRabbitConfig;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;


import javax.annotation.Resource;

@Component
public class RefreshProducer {

    @Resource
    private RabbitTemplate rabbitTemplate;

    public void send(String msg, String routingKey){
        rabbitTemplate.convertAndSend(RabbitConfig.REFRESH_DIRECT_EXCHANGE_NAME,routingKey,msg);
    }
    public void sendMail(String msg, String routingKey){
        rabbitTemplate.convertAndSend(MailRabbitConfig.MAIL_DIRECT_EXCHANGE_NAME,routingKey,msg);
    }

    public void sendSms(String msg,String routingKey){
        rabbitTemplate.convertAndSend(SmsRabbitConfig.SMS_DIRECT_EXCHANGE_NAME,routingKey,msg);
    }
}
