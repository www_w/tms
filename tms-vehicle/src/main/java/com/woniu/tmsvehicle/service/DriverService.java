package com.woniu.tmsvehicle.service;

import com.woniu.tmscommons.entity.Driver;

import java.util.List;

public interface DriverService {
    List<Driver> findAll();

    Driver selectById(Integer id);

    int deleteById(Integer id);

    int insertAMsg(Driver driver);

    int updateAMsg(Driver driver);

    List<Driver> findAvailableDriver();

    List<Driver> findByCondition(Driver driver);

    int fixTheDriveStatus(Integer id);
    int fixTheDriveStatusForMyself(Integer id);

    List<Driver> findOurDrivers();

}
