package com.woniu.tmsvehicle.service;

import com.woniu.tmscommons.entity.PricingRule;
import com.woniu.tmscommons.entity.VehicleType;

import java.math.BigDecimal;
import java.util.List;

public interface PricingRuleService {
    List<PricingRule> findAll();


    int updateThePrice(PricingRule pricingRule);

    VehicleType findDetailOfType(Integer vehicleTypeId);
}
