package com.woniu.tmsvehicle.service;

import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmsvehicle.vo.AddrVo;

import java.util.List;
import java.util.Map;

public interface TransportService {
    List<Driver> findAvailableDriverAndVehicle();

    Map<String,String> findNearestDriverAndVehicle(String ip);

    int findDistance(AddrVo addrVo);


//    Driver findShortestDistance(String latTo, String lngTo);

    Vehicle findRightVehicle(Double capacity,String senderAddress);

    String getTheRightAddr(String lat, String lng);

    void sendEmail(String email);

    void sendSms(String phoneNum);
}
