package com.woniu.tmsvehicle.service;

import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmsvehicle.vo.PageBean;
import com.woniu.tmsvehicle.vo.UpdateStatusVo;

import java.util.List;

public interface VehicleService {

    List<Vehicle> findAll();

    Vehicle selectById(Integer id);

    int deleteById(Integer id);

    int insertOne(Vehicle vehicle);

    int updateAMsg(Vehicle vehicle);


    List<Vehicle> findByCondition(Vehicle vehicle);

    Vehicle findVehicleByUserId(Integer userId);

    int updateStatus(Integer id);

    List<Vehicle> findOurVehicle();

    void updateVehicleAndDriverStatus(UpdateStatusVo updateStatusVo);
}
