package com.woniu.tmsvehicle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmscommons.enums.DriverStatus;
import com.woniu.tmscommons.enums.DriverTypeStatus;
import com.woniu.tmsvehicle.config.RabbitConfig;
import com.woniu.tmsvehicle.mapper.DriverMapper;
import com.woniu.tmsvehicle.producer.RefreshProducer;
import com.woniu.tmsvehicle.service.DriverService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DriverServiceImpl implements DriverService {

    @Resource
    private DriverMapper driverMapper;
    @Resource
    private RefreshProducer refreshProducer;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Override
    public List<Driver> findAll() {
        List<Driver> drivers =(List<Driver>) redisTemplate.opsForValue().get("drivers");
        if(drivers != null){
            return drivers;
        }
        List<Driver> driversDb = driverMapper.selectList(null);
        redisTemplate.opsForValue().set("drivers",driversDb);
        return driversDb;
    }

    @Override
    public Driver selectById(Integer id) {
        Driver driver =(Driver) redisTemplate.opsForValue().get("driver-" + id);
        if(driver != null){
            return driver;
        }
        Driver driverDb = driverMapper.selectById(id);
        redisTemplate.opsForValue().set("driver-" + id,driverDb);
        return driverDb;
    }

    @Override
    public int deleteById(Integer id) {
        return driverMapper.deleteById(id);
    }

    @Override
    public int insertAMsg(Driver driver) {
        int insert = driverMapper.insert(driver);
        driver.setDriverStatus(DriverStatus.AVAILABLE);
        refreshProducer.send("drivers-refresh-driver",RabbitConfig.REFRESH_ROUTING_KEY);
        return insert;
    }

    @Override
    public int updateAMsg(Driver driver) {
        int update = driverMapper.updateById(driver);
        refreshProducer.send("driver-refresh-driver-" + driver.getId() , RabbitConfig.REFRESH_ROUTING_KEY);
        refreshProducer.send("drivers-refresh-driver-" + driver.getId() , RabbitConfig.REFRESH_ROUTING_KEY);
        return update;
    }

    @Override
    public List<Driver> findAvailableDriver() {
        LambdaQueryWrapper<Driver> queryWrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<Driver> eq = queryWrapper.eq(Driver::getDriverStatus, DriverStatus.AVAILABLE);
        List<Driver> drivers = driverMapper.selectList(queryWrapper);
        return drivers;
    }

    @Override
    public List<Driver> findByCondition(Driver driver) {
        LambdaQueryWrapper<Driver> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Driver::getName,driver.getName());
        queryWrapper.like(Driver::getLicenseNumber,driver.getLicenseNumber());
        List<Driver> drivers = driverMapper.selectList(queryWrapper);
        return drivers;
    }


    @Override
    public int fixTheDriveStatus(Integer id) {
        Driver driver = driverMapper.selectById(id);
        DriverStatus driverStatus = driver.getDriverStatus();
        if(driverStatus.equals(DriverStatus.WORKING)){
            driver.setDriverStatus(DriverStatus.AVAILABLE);
            int update = driverMapper.updateById(driver);
            redisTemplate.convertAndSend("driver_vehicle_status","driver:"+id + ":" + DriverStatus.AVAILABLE.getValue());
            refreshProducer.send("drivers-refresh-driver",RabbitConfig.REFRESH_ROUTING_KEY);
            return update;
        }else {
            driver.setDriverStatus(DriverStatus.WORKING);
            refreshProducer.send("drivers-refresh-driver",RabbitConfig.REFRESH_ROUTING_KEY);
            return driverMapper.updateById(driver);
        }
    }

    @Override
    public List<Driver> findOurDrivers() {
        LambdaQueryWrapper<Driver> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Driver::getDriverType, DriverTypeStatus.OUR);
        List<Driver> drivers = driverMapper.selectList(queryWrapper);
        return drivers;
    }

    /**
     * 自用
     * */
    @Override
    public int fixTheDriveStatusForMyself(Integer id) {
        Driver driver = driverMapper.selectById(id);
        DriverStatus driverStatus = driver.getDriverStatus();
        if(driverStatus.equals(DriverStatus.WORKING)){
            driver.setDriverStatus(DriverStatus.AVAILABLE);
            int update = driverMapper.updateById(driver);
            refreshProducer.send("drivers-refresh-driver",RabbitConfig.REFRESH_ROUTING_KEY);
            return update;
        }else {
            driver.setDriverStatus(DriverStatus.WORKING);
            refreshProducer.send("drivers-refresh-driver",RabbitConfig.REFRESH_ROUTING_KEY);
            return driverMapper.updateById(driver);
        }
    }
}
