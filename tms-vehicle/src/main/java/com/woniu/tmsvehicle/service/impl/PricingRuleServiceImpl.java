package com.woniu.tmsvehicle.service.impl;

import com.woniu.tmscommons.entity.PricingRule;
import com.woniu.tmscommons.entity.VehicleType;
import com.woniu.tmsvehicle.mapper.PricingRuleMapper;
import com.woniu.tmsvehicle.mapper.VehicleTypeMapper;
import com.woniu.tmsvehicle.service.PricingRuleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@Service
public class PricingRuleServiceImpl implements PricingRuleService {
    @Resource
    private PricingRuleMapper pricingRuleMapper;

    @Resource
    private VehicleTypeMapper  vehicleTypeMapper;

    @Override
    public List<PricingRule> findAll() {
        List<PricingRule> pricingRules = pricingRuleMapper.selectList(null);
        return pricingRules;
    }

    @Override
    public int updateThePrice(PricingRule pricingRule) {
        return pricingRuleMapper.updateById(pricingRule);
    }

    @Override
    public VehicleType findDetailOfType(Integer vehicleTypeId) {
        VehicleType vehicleType = vehicleTypeMapper.selectById(vehicleTypeId);
        return vehicleType;
    }
}
