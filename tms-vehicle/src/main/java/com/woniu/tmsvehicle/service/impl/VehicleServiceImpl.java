package com.woniu.tmsvehicle.service.impl;

//import com.github.pagehelper.Page;
import com.woniu.tmscommons.entity.Vehicle;
import com.woniu.tmscommons.enums.DriverStatus;
import com.woniu.tmscommons.enums.VehicleStatus;
import com.woniu.tmsvehicle.config.RabbitConfig;
import com.woniu.tmsvehicle.config.SmsRabbitConfig;
import com.woniu.tmsvehicle.mapper.VehicleMapper;
import com.woniu.tmsvehicle.producer.RefreshProducer;
import com.woniu.tmsvehicle.service.DriverService;
import com.woniu.tmsvehicle.service.VehicleService;
//import com.woniu.tmsvehicle.vo.PageBean;
import com.woniu.tmsvehicle.vo.UpdateStatusVo;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//import com.github.pagehelper.PageHelper;
import javax.annotation.Resource;
import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {

    @Resource
    private VehicleMapper vehicleMapper;
    @Resource
    private RefreshProducer refreshProducer;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    @Override
    public List<Vehicle> findAll() {
        List<Vehicle> vehicles =(List<Vehicle>) redisTemplate.opsForValue().get("vehicles");
        if(vehicles != null){
            return vehicles;
        }
        List<Vehicle> vehiclesDb = vehicleMapper.findAll();
        redisTemplate.opsForValue().set("vehicles",vehiclesDb);
        return vehiclesDb;
    }

    @Override
    public Vehicle selectById(Integer id) {
        return vehicleMapper.selectById(id);
    }

    @Override
    public int deleteById(Integer id) {
        return vehicleMapper.deleteById(id);
    }

    @Override
    public int insertOne(Vehicle vehicle) {
        vehicle.setVehicleStatus(VehicleStatus.AVAILABLE);
        int num = vehicleMapper.insert(vehicle);
        return num;
    }

    @Override
    public int updateAMsg(Vehicle vehicle) {
        int update = vehicleMapper.updateById(vehicle);
        return update;
    }

    @Override
    public List<Vehicle> findByCondition(Vehicle vehicle)  {
        List<Vehicle> vehicles = vehicleMapper.findByCondition(vehicle);
        return vehicles;
//        int page = 2;
//        int pageSize = 5;
//        PageHelper.startPage(page,pageSize);
//        Page<Vehicle> pageData = (Page<Vehicle>) vehicleMapper.findByCondition(vehicle);
//
//        PageBean<Vehicle> pageBean = new PageBean<>();
//        pageBean.setData(pageData.getResult()); //数据
//        pageBean.setSizeOfPage(pageSize);
//        pageBean.setSizeOfCurrPage(pageData.getResult().size());
//        pageBean.setPage(page);
//        pageBean.setTotalNum((int) pageData.getTotal());
//        pageBean.setTotalPage(pageData.getPages());
//        return pageBean;
    }

    @Override
    public Vehicle findVehicleByUserId(Integer userId) {
        return vehicleMapper.findVehicleByUserId(userId);
    }

    @Override
    public int updateStatus(Integer id) {
        Vehicle vehicle = vehicleMapper.selectById(id);
        VehicleStatus vehicleStatus = vehicle.getVehicleStatus();
        if(vehicleStatus.equals(VehicleStatus.AVAILABLE)){
            vehicle.setVehicleStatus(VehicleStatus.INSERVICE);
            int update = vehicleMapper.updateById(vehicle);
            refreshProducer.send("vehicles-refresh-driver", RabbitConfig.REFRESH_ROUTING_KEY);
            return update;
        }else{
            vehicle.setVehicleStatus(VehicleStatus.AVAILABLE);
            int update = vehicleMapper.updateById(vehicle);
            refreshProducer.sendSms("SendSms-18684096164", SmsRabbitConfig.SMS_ROUTING_KEY);
            refreshProducer.send("vehicles-refresh-driver", RabbitConfig.REFRESH_ROUTING_KEY);
            return update;
        }
    }

    @Override
    public List<Vehicle> findOurVehicle() {

        return vehicleMapper.findOurVehicle();
    }

    @Resource
    private DriverService driverService;
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateVehicleAndDriverStatus(UpdateStatusVo updateStatusVo) {
        updateStatus(updateStatusVo.getVehicleId());
        driverService.fixTheDriveStatus(updateStatusVo.getDriverId());
        redisTemplate.convertAndSend("driver_vehicle_status","driver:"+updateStatusVo.getDriverId()+":"+ DriverStatus.AVAILABLE.getValue() +":"+updateStatusVo.getVehicleId()+":"+VehicleStatus.AVAILABLE.getValue());
    }
}
