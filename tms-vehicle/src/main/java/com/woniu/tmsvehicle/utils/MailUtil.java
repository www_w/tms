package com.woniu.tmsvehicle.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class MailUtil {


    @Resource
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    String sender;
    public void sendEmail(String receiver){

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            //发送方
            mimeMessageHelper.setFrom(sender);
            //接收方
            mimeMessageHelper.setTo(receiver);
            //邮件主题
            mimeMessageHelper.setSubject("收货通知");
            //设置内容
            mimeMessageHelper.setText("<p>您的货物已送达,请前往目的地收货，详情请点击下方链接</p>"
                    ,true);
            long start = System.currentTimeMillis();
            javaMailSender.send(mimeMessage);
            long end = System.currentTimeMillis();
            System.out.println("发送邮件耗时：" + (end - start) + "ms");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
