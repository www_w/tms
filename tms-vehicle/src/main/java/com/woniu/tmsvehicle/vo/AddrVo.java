package com.woniu.tmsvehicle.vo;

import lombok.Data;

@Data
public class AddrVo {
    private String latFrom;
    private String lngFrom;
    private String latTo;
    private String lngTo;
    private String ip;
}
