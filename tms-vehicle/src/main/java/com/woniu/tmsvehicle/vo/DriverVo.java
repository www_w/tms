package com.woniu.tmsvehicle.vo;

import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.enums.DriverStatus;
import lombok.Data;


@Data
public class DriverVo {
    private Driver driver;
}
