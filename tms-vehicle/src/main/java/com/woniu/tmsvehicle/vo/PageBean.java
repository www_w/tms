package com.woniu.tmsvehicle.vo;

import lombok.Data;

import java.util.List;

@Data
public class PageBean<T> {
    private List<T> data;  // 一页的数据
    private int totalNum;  // 总数量
    private int totalPage;  // 总页码
    private int sizeOfPage; // 分页大小
    private int sizeOfCurrPage; // 当前页的数量
    private int page;  // 当前页码
}
