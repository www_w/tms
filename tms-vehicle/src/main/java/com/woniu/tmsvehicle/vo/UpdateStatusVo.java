package com.woniu.tmsvehicle.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateStatusVo {
    private Integer driverId;
    private Integer vehicleId;
}
