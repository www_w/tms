package com.woniu.tmsvehicle.vo;

import com.woniu.tmscommons.entity.Vehicle;
import lombok.Data;

@Data
public class VehicleVo {
    private Vehicle vehicle;
}
