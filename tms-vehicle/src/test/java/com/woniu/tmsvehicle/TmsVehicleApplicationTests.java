package com.woniu.tmsvehicle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woniu.tmscommons.entity.Driver;
import com.woniu.tmscommons.enums.DriverStatus;
import com.woniu.tmsvehicle.service.DriverService;
import com.woniu.tmsvehicle.service.TransportService;
import com.woniu.tmsvehicle.utils.MailUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@SpringBootTest
class TmsVehicleApplicationTests {

    @Test
    void contextLoads() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://apis.map.qq.com/ws/location/v1/ip?ip=111.201.143.41&key=BG7BZ-ZN3K4-APUU7-FPDV6-CSJ3Z-MIFFK";
        String res = restTemplate.getForObject(url, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Map<String,String> map = objectMapper.readValue(res, Map.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void test(){
        List<Integer> distances = new ArrayList<>();
        distances.add(11);
        distances.add(995);
        distances.add(12);
        distances.add(9);
        for(int i=0;i<distances.size();i++){
            if(distances.get(i) == Collections.min(distances)){
                System.out.println(i);
            }
        }
    }

    @Resource
    private TransportService transportService;
    @Test
    void test01(){
        String lan = "30.66603400";
        String lng = "104.08154100";
        String theRightAddr = transportService.getTheRightAddr(lan, lng);
    }

    /**
     * 测试redis缓存
     * */
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private DriverService driverService;
    @Test
    void RedisTestPut(){
        Driver driver = driverService.selectById(3);
        redisTemplate.opsForValue().set("driver:3",driver);
    }

    @Test
    void RedisTestGet(){
        Driver driver =(Driver) redisTemplate.opsForValue().get("driver:3");
        System.out.println("司机3："+ driver);
    }

    @Test
    void RedisTestUpdate(){
        Driver driver = driverService.selectById(3);
        driver.setDriverStatus(DriverStatus.AVAILABLE);
        driverService.updateAMsg(driver);
        redisTemplate.opsForValue().set("driver:3",driver);
    }

    @Test
    void getMsgFromDb(){
        Driver driver = driverService.selectById(3);
        System.out.println(driver);
    }

    @Resource
    private MailUtil mailUtil;
    @Test
    void MailTest(){
        mailUtil.sendEmail("1735281418@qq.com");
    }
}
